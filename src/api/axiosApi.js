import request from "../util/request";
import axios from "axios";
// eslint-disable-next-line import/no-anonymous-default-export
export default {
  //登录注册
  login(params) {
    return request({
      method: "post",
      url: "/users/login",
      data: params,
    });
  },

  //聊天list
  getChatList() {
    return axios
      .all([
        request({
          method: "get",
          url: "/chatroom/recent_groupTalk/query",
        }),
        request({
          method: "get",
          url: "/chatroom/recent_talk/query",
        }),
      ])
      .then(
        axios.spread((group, talk) => {
          return Promise.resolve([...group, ...talk]);
        })
      );
  },
  getGroupData(params) {
    return request({
      method: "get",
      url: `/chatroom/group/query/${params}`,
    });
  },
  delMessageItem(params) {
    return request({
      method: "delete",
      url: `/chatroom/recent_talk/delete/${params}`,
    });
  },

  //聊天
  // 获取表情包
  getEmoji_user(data) {
    return request({
      method: "post",
      url: "/chatroom/emoji_user/query",
      data: {
        ...data,
        totalNam: "",
      },
    });
  },
  // 保存表情包
  saveEmoji_user(data) {
    return request({
      method: "post",
      url: "/chatroom/emoji_user/save",
      data,
    });
  },
  // 发送群聊消息
  sendGroupMsg(data) {
    return request({
      method: "post",
      url: "/chatroom/group_msg/send",
      data,
    });
  },
  // 发送单聊消息
  sendMessage(data) {
    return request({
      method: "post",
      url: "/chatroom/message/send",
      data,
    });
  },
  // 获取历史单聊消息
  getMoreMessage(params) {
    return request({
      method: "get",
      url: "/chatroom/message/query/history",
      data: params,
    });
  },
  // 获取历史群聊消息
  getMoreGroupMessage(params) {
    return request({
      method: "get",
      url: "/chatroom/group_msg/query/history",
      data: params,
    });
  },
  // 更新单聊的未读消息状态
  updateUnreadMessage(params) {
    return request({
      method: "get",
      url: "/chatroom/message/update_msg_status",
      data: params,
    });
  },
  // 更新群聊的未读消息状态
  updateUnreadGroupMessage(params) {
    return request({
      method: "get",
      url: "/chatroom/group_msg/update/group_msg",
      data: params,
    });
  },

  // addressList
  /**
   *
   * @param {{
   * page:'',
   * size:'',
   *
   * }} params
   * @returns
   */
  getFriendList(params) {
    return request({
      method: "post",
      url: "/chatroom/friendship/query/friends",
      data: {
        ...params,
        totalNam: "",
      },
    });
  },
  getGroupList() {
    return request({
      method: "get",
      url: "/chatroom/group/query/all",
    });
  },
  // 获取好友请求
  getNewFriendInfo(params) {
    return request({
      method: "post",
      url: "/chatroom/friendship/query/info",
      data: {
        ...params,
        totalNam: "",
      },
    });
  },
  deleteFriend(data) {
    return request({
      method: "delete",
      url: "/chatroom/friendship/delete",
      data,
    });
  },
  // 更新好友请求状态
  updateFriendship(data) {
    return request({
      method: "post",
      url: "/chatroom/friendship/update",
      data,
    });
  },
  // 清空好友请求未读消息
  delFriendshipStatus(data) {
    return request({
      method: "get",
      url: "/chatroom/friendship/update/friendship_status",
      data,
    });
  },
  // 查询用户
  findNewFriend(params) {
    return request({
      method: "get",
      url: "/chatroom/user/query/phone/" + params,
    });
  },
  addFriend(data) {
    return request({
      method: "post",
      url: "/chatroom/friendship/save",
      data,
    });
  },
  // 盆友圈
  /**
     * 
     * @param {{
        	"commentId": 0,
        	"commentText": "",
        	"createTime": "",
        	"repliedUserId": 0,
        	"shareId": 0,
        	"updateTime": "",
        	"userId": 0
        }} data 
     * @returns 
     */
  addComment(data) {
    return request({
      url: "/chatroom/comment/add",
      method: "post",
      data: data,
    });
  },
  delComment(commentId) {
    return request({
      url: `/chatroom/comment/delete/${commentId}`,
      method: "delete",
    });
  },
  /**
     * 
     * @param {{
      	"likesId": 0,
      	"shareId": 0,
      	"userId": 0
      }} data 
     * @returns 
     */
  likes(data) {
    return request({
      url: "/chatroom/likes/add",
      method: "post",
      data: data,
    });
  },
  deleteLikes(likesId) {
    return request({
      url: "/chatroom/likes/delete/" + likesId,
      method: "delete",
    });
  },
  deleteCircle(shareId) {
    return request({
      url: "/chatroom/share/delete/",
      method: "get",
      data: shareId,
    });
  },
  /**
     * 
     * @param {{
            "createTime": "",
            "img01": "",
            "img02": "",
            "img03": "",
            "img04": "",
            "img05": "",
            "img06": "",
            "img07": "",
            "img08": "",
            "img09": "",
            "shareId": 0,
            "text": "",
            "updateTime": "",
            "userId": 0
        }} data 
     * @returns 
     */
  addCircle(data) {
    return request({
      url: "/chatroom/share/publish",
      method: "post",
      data: data,
    });
  },
  /**
     * 
     * @param {{
      "page":'',
      "size":''}} data 
     * @returns 
     */
  getCircle(data) {
    return request({
      url: "/chatroom/share/query/self/",
      method: "post",
      data: data,
    });
  },
  postCircle() {
    return request({
      // url:
    });
  },
  // 清空盆友圈通知
  delCircleMsg() {
    return request({
      url: "/chatroom/share/update/sys_msg_status",
      method: "get",
    });
  },
  // 获取盆友圈通知
  getCircleMsg() {
    return request({
      url: "/chatroom/message/query/sys_msg",
      method: "get",
    });
  },
  // 导航栏
  getOfflineMessageNum() {
    return axios.all([
      request({
        url: "/chatroom/message/query/offline_message_num",
        method: "get",
      }),
      request({
        url: "/chatroom/friendship/query/friendship_num",
        method: "get",
      }),
      request({
        url: "/chatroom/share/query/sys_msg_num",
        method: "get",
      }),
    ]);
  },

  // 阿里云
  upDataFile(data) {
    return request({
      url: "/chatroom/file/upload",
      method: "post",
      data: data,
    });
  },
  // 添加最近聊天
  // 添加最近单聊
  setRecentTalk(params) {
    return request({
      url: "/chatroom/recent_talk/add",
      method: "post",
      data: params,
    });
  },
  // 添加最近群聊聊
  setRecentGroupTalk(params) {
    return request({
      url: "/chatroom/recent_groupTalk/add",
      method: "post",
      data: params,
    });
  },
  // 群聊
  addGroup(params) {
    return request({
      url: "/chatroom/group/add",
      method: "post",
      data: params,
    });
  },
  delGroup(params) {
    return request({
      url: "/chatroom/group/delete",
      method: "post",
      data: params,
    });
  },
  // 根据群聊 id 和 群成员的 id 删除群聊中的成员
  /**
     * 
     * @param {{
     * "createTime": "",
     "groupId": 0,
     "groupUserId": 0,
     "updateTime": "",
     "userId": 0}} data 
     * @returns 
     */
  delGroupMember(data) {
    return request({
      url: "/chatroom/group_user/member/delete",
      method: "delete",
      data,
    });
  },
  addGroupUser(data) {
    return request({
      url: "/chatroom/group_user/member/add",
      method: "post",
      data,
    });
  },
  // 登录
  /**
     * 
     * @returns {
	"password": "",
	"username": ""
}
     */
  login(data) {
    return request({
      url: "/chatroom/user/sys/login",
      method: "post",
      data,
    });
  },
  // 注册
  /**
     * 
     * @param {{
	    "icon": "",
	    "password": "",
	    "phone": "",
	    "username": ""
    }} data 
     * 
     */
  register(data) {
    return request({
      url: "/chatroom/user/sys/register",
      method: "post",
      data,
    });
  },
};
