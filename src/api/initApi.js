import store from "../redux/store";
import axiosApi from "./axiosApi";
import {
    initMessageList
} from '../redux/actions/messageList'
import {
    initAddressList
} from '../redux/actions/addressList'


// 接收最近消息
export default () => {
    console.log('inirApi-------------------');
    function sortMessageList(list) {
        list.sort((previous, next) => {
            let previousTime = getGroupOrMessageTime(previous)
            let nextTime = getGroupOrMessageTime(next)
            if (previousTime > nextTime) return 1
            return -1
        })
        return list
    }
    // 获取群聊或单聊的最近消息的创建时间
    function getGroupOrMessageTime(list) {
        return (list.groupMsgList && list.groupMsgList.length && list.groupMsgList[list.groupMsgList.length - 1].createTime) || (list.messageList && list.messageList.length && list.messageList[list.messageList.length - 1].sendTime)
    }
    axiosApi.getChatList().then(res => {
        store.dispatch(initMessageList({
            data: sortMessageList(res)
        }))
    })


    // 通讯录
    const pageOV = {
        page: 1,
        size: 20
    }
    axiosApi.getFriendList(pageOV).then(res => {
        console.log('init--->getFriendList');
        store.dispatch(initAddressList({
            initName: 'address',
            initData: res
        }))
    })
    axiosApi.getGroupList(pageOV).then(res => {
        console.log('init--->getGroupList');
        store.dispatch(initAddressList({
            initName: 'group',
            initData: res
        }))
    })


}