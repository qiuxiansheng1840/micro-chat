import Socket from "../util/socket";
import {
    handleSocketMsg
} from "./handleSocketMsg";
import store from '../redux/store'
import config from "../config";
/**
 * 2000系统消息
 */


function socketFn() {
    const storeData = store.getState()
    console.log(storeData);
    let baseUrl = config.baseUrl.slice(7)
    const socket = new Socket({
        socketUrl: `ws://${baseUrl}/chatroom/websocket/${storeData.userData.id}`,
        timeout: 5000,
        socketMessage: (receive) => {
            console.log(receive); //后端返回的数据，渲染页面
            console.log(JSON.parse(receive.data));
            handleSocketMsg(JSON.parse(receive.data))
        },
        socketClose: (msg) => {
            // console.log(msg);
        },
        socketError: () => {
            console.log('连接建立失败');
        },
        socketOpen: () => {
            console.log('连接建立成功');
            // // 心跳机制 定时向后端发数据
            // this.taskRemindInterval = setInterval(() => {
            //     this.socket.sendMessage({
            //         "msgType": 0
            //     })
            // }, 30000)
        }
    });
    return socket
}
export default socketFn
//  ()=>{

// }
// //重试创建socket连接
// try {
//     socket.connection();
// } catch (e) {
//     // 捕获异常，防止js error
//     // donothing
// }