import store from "../redux/store"
import PubSub from "pubsub-js"
import * as navAction from '../redux/actions/nav'
import * as messageAction from '../redux/actions/messageList'
import {
    createBrowserHistory
} from 'history'
export function handleSocketMsg(socketMsg) {
    const history = createBrowserHistory()

    // console.log();
    // console.log(Route);
    // console.log(navAction);
    // console.log(socketMsg);
    const {
        data
    } = socketMsg
    console.log(data);
    console.log(data.data);
    let pathNameIndex = null;
    let ChatId = null;
    console.log();
    let code = data.sysMsgCode || socketMsg.code
    console.log(code);
    switch (code) {
        // 单聊消息
        case "3000":
            pathNameIndex = history.location.pathname.lastIndexOf('chatList/')
            ChatId = history.location.pathname.slice(pathNameIndex + 9)
            console.log(ChatId);
            if (data.sendId != ChatId) {
                store.dispatch(messageAction.changeUnRead({
                    chatId: data.sendId,
                    changeData: 1,
                }));
                store.dispatch(navAction.addNavUnreadMessages({
                    addChangeName: "chat",
                    addChangeNum: 1
                }))
            }
            store.dispatch(messageAction.editorSend({
                chatId: data.sendId,
                message: data
            }))
            break;
            // 群聊消息
        case "4000":
            pathNameIndex = history.location.pathname.lastIndexOf('group/')
            ChatId = history.location.pathname.slice(pathNameIndex + 6)
            console.log(ChatId);
            if (data.groupId != ChatId) {
                store.dispatch(messageAction.changeUnRead({
                    chatId: data.groupId,
                    changeData: 1,
                    isGroup: true
                }));
                store.dispatch(navAction.addNavUnreadMessages({
                    addChangeName: "chat",
                    addChangeNum: 1
                }))
            }
            store.dispatch(messageAction.editorSend({
                chatId: data.groupId,
                message: data,
                isGroup: true
            }))
            break;
            break;
            // 盆友圈消息
        case "2103":
        case "2102":

            console.log(data);
            const dataText = JSON.parse(data.sysMsgText)
            // if(data.sysMsgDesc=="微广场点赞消息"){

            // }else{

            // }
            store.dispatch(navAction.addNavUnreadMessages({
                addChangeName: "square",
                addChangeNum: 1
            }))
            PubSub.publish('addCMessage', JSON.parse(data.sysMsgText))
            break;
            // 好友请求
        case "2001":
            store.dispatch(navAction.addNavUnreadMessages({
                addChangeName: "addressList",
                addChangeNum: 1
            }))
        default:
            return null
    }



}