// 展示自定义右键事件
export const SHOWRIGHTCLICK = 'showRightClick'
// 关闭自定义右键事件
export const CLOSERIGHTCLICK = 'closeRightClick'

// 初始化聊天列表内容
export const INITMESSAGELIST = 'initMessageList' 
// 修改聊天内容的id
export const CHANGEMESSAGE = 'changeMessageId' 
// 修改输入框内容
export const EDITORCHANGE = 'editorChange' 
// 发送输入框内容
export const EDITORSEND = 'editorSend'
// 发送文件
export const SENDFILE = 'sendfile'
// 添加聊天人
export const ADDCHAT  = 'addChat'
//删除聊天人
export const DELCHAT = 'delChat'
// 修改未读消息数
export const CHANGENUREAD = 'changeUnRead'
//向上查询历史记录
export const ADDHISTORY = 'addHistory'




// 初始化通讯录
export const INITADDRESSLIST = 'initAddressList'
// 删除通讯录某一方
export const DELADDRESSLISTITEM = 'delAddressListItem'
// 添加通讯录的用户
export const ADDADDRESSLISTITEM = 'addAddressListItem'
// 删除群聊群友
export const DELGROUPMEMBER = 'delGroupMember'
// 解散群聊
export const DELGROUP = 'delGroup'
// 添加用户到群聊中
export const ADDGROUPMEMBER ='addGroupMember'



// 初始化导航栏未读消息数
export const INITNAVUNREADMESSAGES = "initNavUnreadMessages"
// 添加导航栏未读消息数
export const ADDNAVUNREADMESSAGES = "addNavUnreadMessages"
// 减少导航栏未读消息数
export const REDNAVUNREADMESSAGES = "redNavUnreadMessages"


// 初始化朋友圈
export const INITCIRCLELIST = "initCircleList"
// 向下查找盆友圈
export const MORECIRCLELIST = "moreCircleList"
// 增加朋友圈
export const ADDCIRCLELIST = "addCircleList"
// 删除盆友圈
export const DELCIRCLELIST = "delCircleList"
// 评论盆友圈
export const COMMENTCIRCLE = "commentCircle"
// 点赞盆友圈
export const LIKECIRCLE = "LikeCircle"
// 取消点赞盆友圈
export const UNLIKECIRCLE = "UnLikeCircle"
// 发送盆友圈
export const SENDCIRCLE = "sendCircleList"
// 改变点赞id
export const CHANGELIKESID = 'changeLikesId'
// 改变评论的id

// 用户信息
export const INITUSERDATA = 'initUserData'





