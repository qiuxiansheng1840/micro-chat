import { createStore } from "redux";
import reducers from './reducers'
//引入redux-devtools-extension
import {composeWithDevTools} from 'redux-devtools-extension'
export default createStore(reducers,composeWithDevTools())
