import { combineReducers } from "redux";
import rightClick from "./rightClick";
import messageList from "./messageList";
import addressList from "./addressList";
import nav from './nav'
import circleList from "./circleList";
import userData from './userData'
export default combineReducers({
    rightClick,
    messageList,
    addressList,
    nav,
    circleList,
    userData
})