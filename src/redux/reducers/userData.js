import {
    INITUSERDATA
} from "../constant";
import storage from '../../util/storage'
const initState = {}
export default function userDataReducers(preState = initState, action) {
    let {
        type,
        data
    } = action
    switch (type) {
        case INITUSERDATA:
            console.log(data);
            storage.setItem('userData', data)
            return {
                ...data
            };
        default:
            if(preState == initState){
                return {}
            }else{
                return storage.getItem('userData')
            }
    }
}