import {
    EDITORCHANGE,
    EDITORSEND,
    ADDCHAT,
    SENDFILE,
    INITMESSAGELIST,
    CHANGENUREAD,
    CHANGEMESSAGE,
    DELCHAT,
    ADDHISTORY
} from "../constant";

let initState = [{
    "recentId": 'initM1',
    "userId": 'initM1',
    "username": "加载中",
    "userIcon": null,
    "receiveId": 'initM1',
    "receiveName": "加载中",
    "receiveIcon": null,
    "offlineMessageNum": 3,
    "messageList": [{
        "messageId": 1,
        "sendId": 2,
        "receiveId": 1,
        "text": "加载中!",
        "sendTime": Date.now(),
        "receiveTime": Date.now(),
        "status": "消息未接收",
        "type": "text"
    }]
}]

// 获取当前的聊天框内容
function findMessageList(messageList, chatId, isGroup) {
    if (isGroup) {
        return messageList.find((value) => {
            return value.groupId == chatId
        }).groupMsgList
    } else {
        return messageList.find((value) => {
            return value.receiveId == chatId
        }).messageList
    }
}

// 获取当前的聊天框内容在initState的位置
function findMessageListIndex(messageList, chatId, isGroup) {
    if (isGroup) {
        return messageList.findIndex((value) => {
            return value.groupId == chatId
        })
    } else {
        return messageList.findIndex((value) => {
            return value.receiveId == chatId
        })
    }

}

// 新增消息
function addMessage(messageList, chatId, message, isGroup) {
    let index = findMessageListIndex(messageList, chatId, isGroup)
    messageList[index].draft = ''
    messageList[index].messageList && messageList[index].messageList.push(message)
    messageList[index].groupMsgList && messageList[index].groupMsgList.push(message)
    // 对应的消息聊天框
    const messageData = messageList.splice(index, 1)
    // 将聊天框置顶
    messageList.unshift(messageData[0])
}

// 新增草稿、修改草稿
function addDraft(messageList, chatId, draft, isGroup) {
    let index = findMessageListIndex(messageList, chatId, isGroup)
    messageList[index].draft = draft
}
// 修改未读数
function changeUnRead(messageList, chatId, changeData, isGroup) {
    let index = findMessageListIndex(messageList, chatId, isGroup)
    if (isGroup) {
        changeData == 0 ? messageList[index].offlineGroupMsgNum = changeData : messageList[index].offlineGroupMsgNum += changeData
    } else {
        changeData == 0 ? messageList[index].offlineMessageNum = changeData : messageList[index].offlineMessageNum += changeData
    }
}

//删除消息
function delMessage(messageList, chatId, message_Id, isGroup) {
    let index = findMessageListIndex(messageList, chatId, isGroup)
    let myMessageList = messageList[index].messageList || messageList[index].groupMsgList
    for (let i = 0; i < myMessageList.length; i++) {
        if (isGroup) {
            if (myMessageList[i].groupMsgId == message_Id) {
                myMessageList.splice(i, 1)
            };
        } else {
            if (myMessageList[i].messageId == message_Id) {
                myMessageList.splice(i, 1)
            };
        }
    }
}
// 添加用户到聊天list里
function addUserInList(messageList, chatId, innerData, isGroup) {
    let flag = messageList.some((item) => {
        if (isGroup) {
            return item.groupId == chatId
        }
        return item.receiveId == chatId
    })
    let changeData = null
    if (flag) {
        let index = findMessageListIndex(messageList, chatId, isGroup)
        changeData = messageList.splice(index, 1)
        messageList.unshift(changeData[0])
    } else {
        messageList.unshift(innerData)
    }
}
// 删除聊天list的item
function delListItem(messageList, chatId, isGroup) {
    let index = findMessageListIndex(messageList, chatId, isGroup)
    index != -1 && messageList.splice(index, 1)
}

function changeMessage(messageList, chatId, oldMessageId, newMessage, isGroup) {
    const myMessageList = findMessageList(messageList, chatId, isGroup)
    // let index = findMessageListIndex(messageList, chatId, isGroup)
    // const myMessageList = messageList[index].messageList ||  messageList[index].groupMsgList
    for (let i = 0; i < myMessageList.length; i++) {
        if (myMessageList[i].messageId == oldMessageId || myMessageList[i].groupMsgId == oldMessageId) {
            // messageList[i].messageId && (messageList[i].messageId = newMessageId)
            // messageList[i].groupMsgId && (messageList[i].groupMsgId = newMessageId)
            myMessageList[i] = {
                ...myMessageList[i],
                ...newMessage
            }
        }
    }
}
export default function messageListReducer(preState = initState, action) {
    let {
        type,
        data
    } = action
    switch (type) {
        case EDITORCHANGE:
            // 输入框内容改变，修改草稿
            const {
                chatId, draft, isGroup
            } = data
            addDraft(preState, chatId, draft, isGroup)
            return [...preState]
        case CHANGEMESSAGE:
            changeMessage(preState, data.chatId, data.oldMessageId, data.newMessage, data.isGroup)
            return [...preState]
        case EDITORSEND:
            console.log(data);
            addMessage(preState, data.chatId, data.message, data.isGroup)
            return [...preState]
        case ADDCHAT:
            const userId = data.userId
            const innerData = data.innerData
            addUserInList(preState, userId, innerData, data.isGroup)
            return [...preState]
        case DELCHAT:
            delListItem(preState, data.chatId, data.isGroup)
            return [...preState]
        case SENDFILE:
            const sendData = data.sendData
            return [sendData, ...preState]
        case INITMESSAGELIST:
            const initData = data.data
            return initData
        case CHANGENUREAD:
            changeUnRead(preState, data.chatId, data.changeData, data.isGroup)
            return [...preState]
        case ADDHISTORY:
            return [...data,...preState]
        default:
            return preState
    }
}