import {
    ADDADDRESSLISTITEM,
    DELADDRESSLISTITEM,
    INITADDRESSLIST,
    DELGROUPMEMBER,
    ADDGROUPMEMBER,
    DELGROUP
} from "../constant";

let initState = {
    address: {
        list: [],
        page: 1,
        size: 20
    },
    group: {
        list: [],
        page: 1,
        size: 20
    }
}

function addList(list, addName, data) {
    list[addName].list = [...list[addName].list, data]
}

function delList(list, delName, id) {
    let index = list[delName].list.findIndex((item) => {
        if (delName == "address") {
            return item.friendshipsId == id
        } else {
            return item.groupId == id
        }
    })
    list[delName].list.splice(index, 1)
}

function deleteGroupMember(list, groupId, id) {
    let groupIndex = list.group.list.findIndex((item) => {
        return groupId == item.groupId
    })
    let userIndex = list.group.list[groupIndex].groupMemberList.findIndex((item) => {
        return id == item.id
    })
    list.group.list[groupIndex].groupMemberList.splice(userIndex, 1)
}
function addGroupMember(list,groupId,data){
    let groupIndex = list.group.list.findIndex((item) => {
        return groupId == item.groupId
    })
    list.group.list[groupIndex].groupMemberList.push(data)
}
function initList(list, initName, initData) {
    list[initName].list = initData
}
export default function addressListReducers(preState = initState, action) {
    const {
        type,
        data
    } = action
    switch (type) {
        case INITADDRESSLIST:
            initList(preState, data.initName, data.initData)
            return {
                ...preState
            }
            // 添加通讯录
            case ADDADDRESSLISTITEM:
                addList(preState, data.addName, data.addData)
                return {
                    ...preState
                };
            case DELADDRESSLISTITEM:
                delList(preState, data.delName, data.delId)
                return {
                    ...preState
                };
            case DELGROUPMEMBER:
                deleteGroupMember(preState, data.groupId, data.userId)
                return {
                    ...preState
                }
            case ADDGROUPMEMBER:
                addGroupMember(preState,data.groupId, data.memberData)
                return {
                    ...preState
                }
            
            default:
                return preState;
    }
}