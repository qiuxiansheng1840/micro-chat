import { SHOWRIGHTCLICK,CLOSERIGHTCLICK } from "../constant";
const initState = {
    x: 0,
    y: 0,
    handleFn: [],
    isShow: false
}
export default function rightClickReducer(preState = initState,action){
    //从action对象中获取：type、data
	const {type,data} = action
    switch(type){
        case SHOWRIGHTCLICK:
            return {...preState,...data}
        case CLOSERIGHTCLICK:
            return {...preState,isShow:false}
        default:
            return preState
    }
}