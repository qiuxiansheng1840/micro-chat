import {
    ADDCIRCLELIST,
    DELCIRCLELIST,
    COMMENTCIRCLE,
    LIKECIRCLE,
    SENDCIRCLE,
    UNLIKECIRCLE,
    MORECIRCLELIST,
    INITCIRCLELIST,
    CHANGELIKESID
} from '../constant'
// const initState = [{
//         shareId: 'c456789',
//         name: 'lili',
//         user_id: 'u456',
//         text: '4567945123',
//         img_01: '',
//         img_02: '',
//         img_03: '',
//         img_04: '',
//         img_05: '',
//         img_06: '',
//         img_07: '',
//         img_08: '',
//         img_09: '',
//         create_time: '1636279312785',
//         commentList: [{
//             comment_id: 'com45678989',
//             user_id: 789456,
//             user_name: 'lili',
//             replied_user_id: 'u789',
//             replied_name: 'kiki',
//             create_time: '',
//             update_time: '',
//             comment_text: '456789'
//         }],
//         likesList: [{
//                 likes_id: '456789',
//                 user_id: 'uqq',
//                 user_name: '789'
//             },
//             {
//                 likes_id: 'l45678998',
//                 user_id: 'uq',
//                 user_name: '789456'
//             }
//         ]
//     },
//     {
//         shareId: 'c45674589',
//         user_id: 'u456',
//         text: '4567945123',
//         name: 'lili',
//         img_01: '钢铁侠头像.jpeg',
//         img_02: '',
//         img_03: '',
//         img_04: '',
//         img_05: '',
//         img_06: '',
//         img_07: '',
//         img_08: '',
//         img_09: '',
//         create_time: '1636279312785',
//         commentList: [{
//             comment_id: 'com456789',
//             user_id: 789456,
//             user_name: 'lili',
//             replied_user_id: 'u7859',
//             create_time: '',
//             update_time: '',
//             comment_text: '4578945612'
//         }],
//         likesList: [{
//             likes_id: 'l778',
//             user_id: '',
//             user_name: '789'
//         }]
//     }
// ]

// 获取当前的盆友圈在initState的位置
function findIndex(list, shareId) {
    return list.findIndex((value) => {
        return value.shareId == shareId
    })
}

function delCircle(list, id) {
    let index = findIndex(list, id)
    list.splice(index, 1)
}

function commentOrLikeCircle(list, id, changData, data) {
    let index = findIndex(list, id)
    list[index][changData].push(data)
}

function unLikeCircle(list, shareId, user_id) {
    let cIndex = findIndex(list, shareId)
    let lIndex = list[cIndex].likesList.findIndex((value) => {
        return value.userId == user_id
    })
    list[cIndex].likesList.splice(lIndex, 1)
}

function changeLikesId(list,shareId,oldId,newID){
  const index = findIndex(list,shareId)
  const likesList = list[index].likesList || []
  likesList.forEach(element => {
    if(element.likesId==oldId){
      element.likesId = newID
    }
  });
  
}
export default function circleListReducers(preState = [], action) {
    const {
        type,
        data
    } = action
    switch (type) {
        case INITCIRCLELIST:
            const initList = data.initList
            return initList
        case MORECIRCLELIST:
            const moreCircleList = data.moreCircleList
            return [...preState,...moreCircleList]
        case ADDCIRCLELIST:
            return [data, ...preState]
        case DELCIRCLELIST:
            const delShare_id = data.shareId
            delCircle(preState, delShare_id)
            return [...preState]
        case COMMENTCIRCLE:
            const comShare_id = data.shareId
            const commentData = data.commentData
            commentOrLikeCircle(preState, comShare_id, 'commentList', commentData)
            return [...preState]
        case LIKECIRCLE:
            const likeData = data.likeData
            const likeShare_id = data.shareId
            commentOrLikeCircle(preState, likeShare_id, 'likesList', likeData)
            return [...preState]
        case UNLIKECIRCLE:
            const unLikeShare_id = data.shareId
            const user_id = data.user_id
            unLikeCircle(preState, unLikeShare_id, user_id)
            return [...preState]
        case SENDCIRCLE:
            return [data, ...preState]
        case CHANGELIKESID:
          const CLI_shareId = data.shareId
          const CLI_oldId = data.oldId
          const CLI_newId = data.newId
          changeLikesId(preState,CLI_shareId,CLI_oldId,CLI_newId)
          return [...preState]
        default:
            return preState;
    }
}