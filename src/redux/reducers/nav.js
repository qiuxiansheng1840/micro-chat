import {
    ADDNAVUNREADMESSAGES,
    REDNAVUNREADMESSAGES,
    INITNAVUNREADMESSAGES
} from "../constant";
const initState = {
    chat: 0,
    addressList: 0,
    square: 0
}
export default function navReducers(preState = initState, action) {
    let {
        type,
        data
    } = action
    switch (type) {
        case INITNAVUNREADMESSAGES:
            return {
                ...data
            };
        case ADDNAVUNREADMESSAGES:
            let {addChangeName,addChangeNum} = data
            preState[addChangeName] += addChangeNum
            return {
                ...preState
            };
        case REDNAVUNREADMESSAGES:
            let {redChangeName,redChangeNum} = data
            preState[redChangeName] -= redChangeNum
            if(preState[redChangeName]<0){
                preState[redChangeName]=0
            }
            return {
                ...preState
            };
        default:
            return preState
    }
}