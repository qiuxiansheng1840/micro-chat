import { ADDNAVUNREADMESSAGES,REDNAVUNREADMESSAGES,INITNAVUNREADMESSAGES } from "../constant";
// 初始化导航栏未读消息数
export const initNavUnreadMessages = data=>({type:INITNAVUNREADMESSAGES,data})
// 添加导航栏未读消息数
export const addNavUnreadMessages = data=>({type:ADDNAVUNREADMESSAGES,data})
// 减少导航栏未读消息数
export const redNavUnreadMessages =  data=>({type:REDNAVUNREADMESSAGES,data})