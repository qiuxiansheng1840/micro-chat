import {
    ADDCIRCLELIST,
    DELCIRCLELIST,
    COMMENTCIRCLE,
    LIKECIRCLE,
    SENDCIRCLE,
    UNLIKECIRCLE,
    INITCIRCLELIST,
    MORECIRCLELIST,
    CHANGELIKESID
} from '../constant'

export const initCircleList = (data) => ({
    type: INITCIRCLELIST,
    data
})
export const moreCircleList = (data) => ({
    type: MORECIRCLELIST,
    data
})
export const addCircleList = (data) => ({
    type: ADDCIRCLELIST,
    data
})
export const delCircleList = data => ({
    type: DELCIRCLELIST,
    data
})
export const commentCircle = data => ({
    type: COMMENTCIRCLE,
    data
})
export const LikeCircle = data => ({
    type: LIKECIRCLE,
    data
})
export const unLikeCircle = data => ({
    type: UNLIKECIRCLE,
    data
})
export const sendCircleList = data => ({
    type: SENDCIRCLE,
    data
})
// export const changeLikesId = data => ({
//     type: CHANGELIKESID,
//     data
// })