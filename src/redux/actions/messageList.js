import { EDITORCHANGE,EDITORSEND,ADDCHAT,SENDFILE,INITMESSAGELIST,CHANGENUREAD,CHANGEMESSAGE,DELCHAT ,ADDHISTORY} from "../constant";

export const changeMessage = data=>({type:CHANGEMESSAGE,data})
export const addHistory = data=>({type:ADDHISTORY,data})



// 修改未读消息数
export const changeUnRead = data=>({type:CHANGENUREAD,data})
// 初始化聊天列表内容
export const initMessageList = data=>({type:INITMESSAGELIST,data})
//  修改输入框内容
export const editorChange = data=>({type:EDITORCHANGE,data})
// 发送输入框内容
export const editorSend = data=>({type:EDITORSEND,data})
// 添加聊天人
export const addChat = data=>({type:ADDCHAT,data})
// 发送文件
export const sendMessage = data=>({type:SENDFILE,data})
//删除聊天人
export const delChat = data=>({type:DELCHAT,data})



