import { ADDADDRESSLISTITEM,DELADDRESSLISTITEM,INITADDRESSLIST,DELGROUPMEMBER,DELGROUP,ADDGROUPMEMBER } from "../constant";
export const initAddressList = data=>({type:INITADDRESSLIST,data})
export const delAddressBookItem = data=>({type:DELADDRESSLISTITEM,data})
export const addAddressBookItem = data=>({type:ADDADDRESSLISTITEM,data})
export const delGroupMember = data=>({type:DELGROUPMEMBER,data})
export const addGroupMember = data=>({type:ADDGROUPMEMBER,data})
export const delGroup = data=>({type:DELGROUP,data})