import React, { Component } from 'react'
import { Form, Input, Button, Radio, Checkbox, Upload, message } from 'antd';
import ImgCrop from 'antd-img-crop';
import { Link } from 'react-router-dom';
import './signIn.scss'
import util from '../../util/util';
import sparkMd5 from 'spark-md5';
import axiosApi from '../../api/axiosApi';

export default class SignIn extends Component {
    componentDidMount() {
        console.log(this.props);
        console.log(this.form);
    }
    state = { fileList: [] }
    onChange = ({ fileList }) => {
        const file = fileList[fileList.length - 1]
        file.status = 'dome'
        console.log(fileList);
        this.setState({ fileList })
    };
    onFinish = (values) => {
        const file = this.state.fileList[0]
        util.getBase64(file.originFileObj).then(res => {
            file.md5 = sparkMd5.hash(res)
            const fileData = new FormData()
            fileData.append('file', file.originFileObj)
            fileData.append('fileId', 0)
            fileData.append('fileKey', file.md5)
            fileData.append('fileName', file.name)
            fileData.append('fileType', 'image')
            fileData.append('fileSuffix', file.name.slice(file.name.lastIndexOf('.')))
            fileData.append('groupMsgId', 0)
            fileData.append('receiverId', 0)
            fileData.append('senderId', 0)
            fileData.append('createTime', Date.now().toString())
            axiosApi.upDataFile(fileData).then(res => {
                console.log(res);
                axiosApi.register({
                    icon:res.filePath,
                    password:values.password,
                    phone:values.phone,
                    username:values.username
                }).then(res=>{
                    message.info('注册成功')
                    this.form.resetFields()
                    this.props.history.push(`/login`)
                })
            })

        })

        console.log('Success:', values);
    }
    // 自定义的校验规则
    checkHeader = () => {
        if (this.state.fileList.length) {
            return Promise.resolve();
        } else if (!this.state.fileList.length) {
            return Promise.reject(new Error('头像未上传'));
        }
    }
    // 第二次使用json
    render() {
        return (
            <div className='signIn'>
                <div className="from">
                    <Form
                        ref={c => this.form = c}
                        name="basic"
                        labelCol={{ span: 5 }}
                        wrapperCol={{ span: 20 }}
                        onFinish={this.onFinish}
                        autoComplete="off"
                        labelAlign="right"
                    >
                        <Form.Item
                            required={true}
                            label="头像"
                            name="header"
                            messageVariables={this.state.fileList.length}
                            rules={[{ validator: this.checkHeader }]}
                            messageVariables={this.state.fileList}
                        >
                            <ImgCrop rotate>
                                <Upload
                                    accept=".png, .jpg, .jpeg"
                                    listType="picture-card"
                                    fileList={this.state.fileList}
                                    onChange={this.onChange}
                                    // onPreview={onPreview}
                                    maxCount={1}
                                >
                                    {this.state.fileList.length < 1 && '+ 上传头像'}
                                </Upload>
                            </ImgCrop>
                        </Form.Item>

                        <Form.Item
                            label="用户名"
                            name="username"
                            rules={[{ required: true, message: '请输入用户名' }]}
                        >
                            <Input />
                        </Form.Item>
                        <Form.Item
                            label="电话号码"
                            name="phone"
                            rules={[{ required: true, message: '请输入电话号码' }]}
                        >
                            <Input />
                        </Form.Item>

                        <Form.Item
                            label="密码"
                            name="password"
                            rules={[{ required: true, message: '请输入密码' }]}
                        >
                            <Input.Password />
                        </Form.Item>

                        {/* <Form.Item
                            label="性别"
                            name="sex"
                            rules={[{ required: true, message: '请选择性别' }]}
                        >
                            <Radio.Group>
                                <Radio.Button value="0">男</Radio.Button>
                                <Radio.Button value="1">女</Radio.Button>
                            </Radio.Group>
                        </Form.Item> */}
                        <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
                            <Button ghost type="primary" htmlType="submit">
                                注册
                            </Button>
                            <Link to="/login">
                                <Button ghost style={{ margin: '0 8px' }} type="primary">
                                    返回登录
                                </Button>
                            </Link>
                        </Form.Item>
                    </Form>
                </div>
            </div>
        )
    }
}
