import React, { Component } from 'react'
import { SmileOutlined } from '@ant-design/icons';
import { Popover } from 'antd';
import Emoji from '../../../components/Emoji/Emoji'
import './circleEmoji.scss'
export default class CircleEmoji extends Component {
    render() {
        const {placement} = this.props
        const emoji = <div id='circleEmoji'><Emoji {...this.props}/></div> 
        return (
            <div className='circleEmoji'>
                <Popover trigger="click" placement={placement} content={emoji} trigger="click">
                    <SmileOutlined />
                </Popover>
            </div>
        )
    }
}
