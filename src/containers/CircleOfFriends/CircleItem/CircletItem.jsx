import React, { Component, Fragment } from 'react';
import { HeartOutlined, HeartTwoTone, MessageOutlined, SmileOutlined } from '@ant-design/icons';
import { Image, message, Spin } from 'antd';
import CircleEmoji from '../CircleEmoji/CircleEmoji';
import Edit from '../../../components/Edit/Edit'
import util from '../../../util/util';
import './circletItem.scss'
import { connect } from 'react-redux';
import { LikeCircle, commentCircle, unLikeCircle } from '../../../redux/actions/circleList'
import storage from '../../../util/storage';
import axiosApi from '../../../api/axiosApi';
import axios from 'axios';
import UUID from 'uuidjs'
import config from '../../../config';
import emojiList from '../../../util/emojiList';
class CircletItem extends Component {
    myId = null
    repliedUsername = null
    repliedId = 0
    componentDidMount() {
        this.circletItem.onclick(this.circletItem)
        this.myId = this.props.userData.id
    }
    state = { showEdit: false, placeholder: "", isLikeLoading: false }
    // 模拟的emoji数据
    list = (length, url) => {
        var res = []
        for (let index = 0; index < length; index++) {
            res.push(url)
        }
        return res
    }
    emojiClickEvent = (emojiCode) => {
        this.Edit.emojiClick(util.emojiCodeToHTML(emojiCode))
    }
    emojiData = {
        itemWidth: "2vw",
        itemHeight: "2vw",
        imgWidth: "80%",
        imgHeight: "auto",
        code: emojiList,
        url: (code) => config.emojiUrl + code,
        clickEvent: this.emojiClickEvent
    }
    likesId = null
    // 判断我是否给盆友圈点赞了
    isLike = (list = [], myId) => {
        return list.filter((item) => {
            return item.userId == myId
        }).length
    }
    // 获取我的点赞likesId
    getMyLikesId() {
        const { showData } = this.props
        return showData.likesList.filter((item) => {
            return item.userId == this.myId
        })[0].likesId
    }

    // 关闭编辑框
    closeEdit = () => {
        this.setState({ showEdit: false })
    }
    // 打开编辑框
    openEdit = (e, placeholder) => {
        e.stopPropagation()
        this.setState({ showEdit: true, placeholder })
        setTimeout(() => {
            this.Edit.focusEnd()
        })
    }
    // 点赞
    LikeCircle = () => {
        this.setState({ isLikeLoading: true })
        const { showData, userData } = this.props
        axiosApi.likes({
            likesId: '',
            shareId: showData.shareId,
            userId: this.myId,
        }).then((res) => {
            this.props.LikeCircle({
                shareId: showData.shareId,
                likeData:
                {
                    likesId: res.likesId,
                    userId: res.userId,
                    username: userData.username
                }
            })
            this.setState({ isLikeLoading: false })
        })

    }
    //取消点赞
    unLikeCircle = () => {
        const { showData } = this.props
        this.setState({ isLikeLoading: true })
        axiosApi.deleteLikes(this.getMyLikesId()).then(() => {
            this.props.unLikeCircle({ shareId: showData.shareId, userId: this.myId })
        })
        this.setState({ isLikeLoading: false })
    }
    // 发送数据
    sendComment = () => {
        const { userData } = this.props
        if (util.HTMLIsNull(this.Edit.getInnerHTML())) {
            const replied_name = this.state.placeholder.slice(2)
            console.log(replied_name);
            const commentData = {
                commentId: null,
                userId: this.myId,
                // 到时候好要获取
                username: userData.username,
                repliedUserId: this.repliedId,
                repliedUsername: replied_name,
                createTime: null,
                updateTime: null,
                shareId: this.props.showData.shareId,
                commentText: util.HTMLToString(this.Edit.getInnerHTML())
            }
            const sendData = { ...commentData }
            delete sendData.repliedUsername
            delete sendData.username
            axiosApi.addComment(sendData).then(res => {
                this.props.commentCircle({ shareId: this.props.showData.shareId, commentData: { ...commentData, ...res, repliedUsername: replied_name } })
                this.closeEdit()
            })
        } else {
            message.info('不可以发表空消息')
        }

    }
    // 传递给编辑编辑器的数据
    editDate = {
        className: "CIM_EditBox",
        send: this.sendComment
    }
    render() {
        const { showData, userData } = this.props
        const myId = userData.id
        const imgList = (() => {
            let list = []
            for (let i = 1; i < 10; i++) {
                if (showData[`img0${i}`]) {
                    list.push(showData[`img0${i}`])
                }
            }
            return list
        })()

        return (
            <div className='circleItem' onClick={this.closeEdit} ref={c => this.circletItem = c} >
                {/* 左边的头像 */}
                <div className='circleItemPortrait'>
                    <div>
                        <img src={showData.userIcon} alt="" />
                    </div>
                </div>
                {/* 右边的主体内容 */}
                <div className='ItemMain'>
                    {/* 分享的文字及图片 */}
                    <div className='circleItemMain'>
                        {/* 发送人名字 */}
                        <div className='CIM_userName' >
                            {showData.username}
                        </div>
                        {/* 发送内容 */}
                        <div className='CIM_text' dangerouslySetInnerHTML={{ __html: util.stringToHTML(showData.text) }}>
                        </div>
                        {/* 发送的图片 */}
                        <div className='CIM_main' >
                            <div className="CIM_img">
                                {
                                    (() => {
                                        if (imgList.length == 0) {
                                            return ''
                                        }
                                        if (imgList.length == 1) {
                                            return (<div className='CIM_onlyOneImg'>
                                                <Image width={"100%"} height={"100%"} src={imgList[0]} alt="" />
                                            </div>)
                                        }
                                        return (<Image.PreviewGroup>
                                            {
                                                imgList.map((item, index) => {
                                                    return <div key={index + 'ig'} className='CIM_singleImg'>
                                                        <Image width={"100%"} height={"100%"} src={item} alt="" />
                                                    </div>
                                                })
                                            }
                                        </Image.PreviewGroup>
                                        )


                                    })()
                                }
                            </div>
                        </div>
                        {/* 时间、点赞和评论 */}
                        <div className='CIM_timeAndLike'>
                            <span className='CIM_time'>{util.CalculationTimeDifferenceForCircle(showData.createTime)}</span>
                            <span className='CIM_likeAndCom'>
                                {
                                    this.state.isLikeLoading ?
                                        <Spin /> :
                                        <>
                                            {
                                                this.isLike(showData.likesList, myId) ?
                                                    <HeartTwoTone onClick={this.unLikeCircle} twoToneColor="#eb2f96" title='取消点赞' /> :
                                                    <HeartOutlined title='点赞' onClick={this.LikeCircle} />
                                            }
                                        </>
                                }

                                <MessageOutlined title='评论' onClick={(e) => { this.openEdit(e, '评论'); this.repliedUsername = ""; this.repliedId = 0 }} />
                            </span>
                        </div>
                        {/* 点赞评论 */}
                        <div className="CIM_likeAndComment">
                            {/* 点赞 */}
                            {
                                showData.likesList.length == 0 ? '' : (
                                    <div className='CIM_likes'>
                                        <HeartTwoTone twoToneColor="#eb2f96" />
                                        {
                                            showData.likesList.map((item, index) => {
                                                return <span key={item.likesId}>{index == showData.likesList.length - 1 ? item.username : item.username + '、'}</span>
                                            })
                                        }
                                    </div>
                                )
                            }
                            {/* 评论 */}
                            <div className='CIM_comments'>
                                {
                                    showData.commentList.map((item) => {
                                        return (
                                            <Fragment key={item.commentId}>
                                                <span className='CIM_commentsName'>{item.username}</span>
                                                {item.repliedUsername ? <>
                                                    <span>&nbsp;回复&nbsp;</span>
                                                    <span className='CIM_commentsName'>{item.repliedUsername}</span>
                                                </>
                                                    : ''}
                                                <span>&nbsp;:&nbsp;</span>
                                                {/* 评论的内容 */}
                                                <span className='CIM_commentsText' onClick={(e) => { this.openEdit(e, '回复' + item.username); this.repliedUsername = item.username; this.repliedId = item.userId }} dangerouslySetInnerHTML={{ __html: util.stringToHTML(item.commentText) }}></span>
                                                <br />
                                            </Fragment>
                                        )
                                    })
                                }
                            </div>
                            {
                                this.state.showEdit ? (<>
                                    {/* 编辑框 */}
                                    <div className='CIM_Edit' onClick={(e) => { e.stopPropagation() }}>
                                        <Edit ref={c => this.Edit = c} placeholder={this.state.placeholder}  {...this.editDate} />
                                        {/* <div className='CTM_bg'>45789</div> */}
                                        <div className='CIM_EditExpression'>
                                            {/* <SmileOutlined /> */}
                                            <CircleEmoji placement="top" {...this.emojiData} />
                                        </div>

                                        <div className='CIM_EditSend' onClick={this.sendComment}>发送</div>
                                    </div>
                                </>) : ''
                            }

                        </div>
                    </div>
                </div>

            </div>
        );
    }
}

export default connect(
    state => ({
        userData: state.userData
    }),
    {
        LikeCircle,
        unLikeCircle,
        commentCircle,
    }
)(CircletItem);