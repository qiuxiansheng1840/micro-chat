import React, { Component } from 'react'
import { HeartOutlined } from '@ant-design/icons';
import './circleMessage.scss'
import withScroll from '../../with/withScroll'
class CircleMessage extends Component {
    render() {
        const { message } = this.props
        const messageLength = message.length
        console.log(message);
        return (
            <div>
                {
                    messageLength ? (message.map((item) => {
                        return <div className='circleMessage' key={item.commentId || item.likesId} >
                            <div div className='circleMessageItem' >
                                {/* <div className='CM_HeadPortrait'>
                            <div>
                                <img src="/img/钢铁侠头像.jpeg" alt="" />
                            </div>
                        </div> */}
                                <div className='CM_main'>
                                    <div className='CM_mainName'>
                                        <span className='CM_name'>
                                            {item.username}
                                        </span>
                                        {/* <span className='CM_time'>
                                    12.30
                                </span> */}
                                    </div>
                                    <div className='CM_text'>
                                        {/* <HeartOutlined/> */}
                                        <span>{item.commentId ? ('评论了你 ' + item.commentText) : "给你点赞了"}</span>
                                    </div>

                                </div>
                                {/* <div className='CM_circle'>
                                    <div>
                                        <img src="/img/钢铁侠头像.jpeg" alt="" />
                                        <span>大家好</span>
                                    </div>
                                </div> */}
                            </div >
                        </div >
                    })) : '还没有消息'
                }
            </div>
        )
    }
}
export default withScroll(CircleMessage)