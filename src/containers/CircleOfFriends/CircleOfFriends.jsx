import React, { Component } from 'react';
import { AlertOutlined, SyncOutlined, HomeOutlined, InstagramOutlined } from '@ant-design/icons';
import { Popover } from 'antd';
import { connect } from 'react-redux';
import './circleOfFriends.scss'
import withMove from '../with/withMove';
import withScroll from '../with/withScroll';
import CircletItem from './CircleItem/CircletItem';
import CircleSend from './CircleSend/CircleSend';
import CircleMessage from './CircleMessage/CircleMessage';
import axiosApi from '../../api/axiosApi';
import { initCircleList, moreCircleList } from '../../redux/actions/circleList'
import { redNavUnreadMessages } from '../../redux/actions/nav'
import PubSub from 'pubsub-js';
class CircleOfFriends extends Component {
    initData = {
        page: 1,
        size: 15,
        totalNum: '',

        // data: null
    }
    startShareId = null
    pageObj = { ...this.initData }
    state = { showHome: false, isLoading: false, message: [] }
    componentDidMount() {
        this.getCircleList()
        axiosApi.getCircleMsg().then(res => {
            res.forEach(item => {
                this.setState({ message: [JSON.parse(item.sysMsgText), ...this.state.message] })
            });
            console.log(res);
        })
        PubSub.unsubscribe('addCMessage') //销毁订阅 防止多次触发
        PubSub.subscribe('addCMessage', (_, data) => {
            this.setState({ message: [data, ...this.state.message] })
        })
    }
    componentWillUnmount() {
        PubSub.unsubscribe('addCMessage') //销毁订阅 防止多次触发

    }
    getCircleList = () => {
        this.pageObj = { ...this.initData }
        this.flag = true
        axiosApi.getCircle(this.pageObj).then(res => {
            this.setState({ isLoading: false })
            this.props.initCircleList({ initList: res.data })
        })
    }
    // 刷新list
    refreshCircleLis = () => {
        this.setState({ isLoading: true })
        this.pageObj = { ...this.initData }
        this.getCircleList()
    }

    
    flag = true
    // 向下添加盆友圈
    scrollButton = () => {
        if (this.flag) {
            this.flag = false
            this.pageObj.page++
            axiosApi.getCircle(this.pageObj).then(res => {
                console.log(res);
                if (res.data.length != 0) {
                    this.props.moreCircleList({ moreCircleList: res.data })
                    this.flag = true
                }
            })
        }

    }
    clearNav = () => {
        axiosApi.delCircleMsg()
        this.props.redNavUnreadMessages({ redChangeName: "square", redChangeNum: this.props.nav.square })
    }
    render() {
        // 使消息有滚动条
        const message = (<div style={{ width: "20vw", height: "45vh" }}><CircleMessage message={this.state.message} /></div>)
        const { showHome, isLoading } = this.state
        const { circleList, userData } = this.props
        return (
            <div id='CircleOfFriends'>
                {/* 提示操作的导航条 */}
                <div id='headerHandle'>
                    <div>
                        <Popover placement="bottomLeft" content={message} trigger="click">
                            <AlertOutlined onClick={this.clearNav} />
                        </Popover>
                        {/* spin是否有旋转动画 */}
                        <SyncOutlined onClick={this.refreshCircleLis} spin={isLoading ? true : false} />
                        {showHome ? <HomeOutlined title='主界面' /> : ''}
                    </div>
                    <div>
                        <Popover placement="bottomRight" content={<CircleSend getCircleList={this.getCircleList} />} trigger="click">
                            <InstagramOutlined title='发布盆友圈' />
                        </Popover>
                    </div>
                </div>
                {/* 背景大图 */}
                <div id="circleOfFriendsHeader">
                    <img src={userData.icon} alt="" />
                    <div className='circleUserData'>
                        <div id='circleHeadPortrait'>
                            <img src={userData.icon} alt="" />
                        </div>
                        <div id='circleUserName'>{userData.username}</div>
                    </div>
                </div>
                <div id="circleOfFriendsMain">
                    {
                        circleList.length ? circleList.map((item) => {
                            return <CircletItem key={item.shareId} showData={{ ...item }} />
                        })
                            : <div className='noMain'>--没有新内容--</div>
                    }

                </div>
            </div>
        );
    }
}


export default connect(
    state => ({
        circleList: state.circleList,
        userData: state.userData,
        nav: state.nav
    }),
    {
        initCircleList,
        redNavUnreadMessages,
        moreCircleList
    }
)(withMove(withScroll(CircleOfFriends))) 