import React, { Component } from 'react'
import { Upload, message } from 'antd';
import { PlusOutlined } from '@ant-design/icons';
import Edit from '../../../components/Edit/Edit';
import CircleEmoji from '../CircleEmoji/CircleEmoji';
import util from '../../../util/util';
import './circleSend.scss'
import storage from '../../../util/storage';
import axiosApi from '../../../api/axiosApi';
import sparkMd5 from 'spark-md5';
import { Spin } from 'antd';
import axios from 'axios';
import emojiList from '../../../util/emojiList';
import config from '../../../config';
import { connect } from 'react-redux';
class CircleSend extends Component {
    state = {
        // fileList: [
        //     {
        //         uid: '-1',
        //         name: 'image.png',
        //         status: 'done',
        //         url: 'https://zos.alipayobjects.com/rmsportal/jkjgkEfvpUPVyRjUImniVslZfWPnJuuZ.png',
        //     },
        //     {
        //         uid: '-2',
        //         name: 'image.png',
        //         status: 'done',
        //         url: 'https://zos.alipayobjects.com/rmsportal/jkjgkEfvpUPVyRjUImniVslZfWPnJuuZ.png',
        //     },
        //     {
        //         uid: '-3',
        //         name: 'image.png',
        //         status: 'done',
        //         url: 'https://zos.alipayobjects.com/rmsportal/jkjgkEfvpUPVyRjUImniVslZfWPnJuuZ.png',
        //     },
        //     {
        //         uid: '-4',
        //         name: 'image.png',
        //         status: 'done',
        //         url: 'https://zos.alipayobjects.com/rmsportal/jkjgkEfvpUPVyRjUImniVslZfWPnJuuZ.png',
        //     },
        //     {
        //         uid: '-xxx',
        //         percent: 50,
        //         name: 'image.png',
        //         status: 'uploading',
        //         url: 'https://zos.alipayobjects.com/rmsportal/jkjgkEfvpUPVyRjUImniVslZfWPnJuuZ.png',
        //     },
        //     {
        //         uid: '-5',
        //         name: 'image.png',
        //         status: 'error',
        //     },
        // ],
        fileList: [],
        loading: false
    };
    beforeUpload = (file) => {
        const isJpgOrPng = file.type === 'image/jpeg' || file.type === 'image/png';
        if (!isJpgOrPng) {
            message.error('You can only upload JPG/PNG file!');
        }
        const isLt2M = file.size / 1024 / 1024 < 2;
        if (!isLt2M) {
            message.error('Image must smaller than 2MB!');
        }
        return isJpgOrPng && isLt2M;
    }
    handleChange = ({ file, fileList }) => {
        util.getBase64(file.originFileObj).then(res => {
            fileList[fileList.length - 1].md5 = sparkMd5.hash(res)
            file = fileList[fileList.length - 1]
            if (file && this.beforeUpload(file)) {
                file.status = ''
                this.setState({ fileList });
            }
            file || this.setState({ fileList });
        })

    }
    // customRequest = () => {

    // }
    emojiClickEvent = (emojiCode) => {
        this.Edit.emojiClick(util.emojiCodeToHTML(emojiCode))
    }
    sendImg = () => {
        let list = []
        let type = 'image'
        this.state.fileList.forEach(file => {
            const fileData = new FormData()
            fileData.append('file', file.originFileObj)
            fileData.append('fileId', 0)
            fileData.append('fileKey', file.md5)
            fileData.append('fileName', file.name)
            fileData.append('fileType', type)
            fileData.append('fileSuffix', file.name.slice(file.name.lastIndexOf('.')))
            fileData.append('groupMsgId', 0)
            fileData.append('receiverId', 0)
            fileData.append('senderId', 0)
            fileData.append('createTime', Date.now().toString())
            list.push(axiosApi.upDataFile(fileData))
        })
        return axios.all(list)
    }
    addImg = (obj, list) => {
        for (let i = 1; i < 10; i++) {
            obj['img0' + i] = list[i - 1] ? list[i - 1].filePath : null
        }
        return obj
    }
    sendCircle = () => {
        const {userData} = this.props
        if (util.HTMLIsNull(this.Edit.getInnerHTML())) {
            let sendData = {
                createTime: null,
                shareId: 0,
                text: util.HTMLToString(this.Edit.getInnerHTML()),
                updateTime: null,
                userId: userData.id
            }
            this.setState({ loading: true })
            this.sendImg().then(res => {
                sendData = this.addImg(sendData, res)
                axiosApi.addCircle(sendData).then(res => {
                    this.Edit.clear()
                    this.setState({ loading: false, fileList: [] })
                    this.props.getCircleList()
                })
            })
        } else {
            message.info('不可以发表空消息')
        }

    }
    customRequest = () => { }
    render() {
        const { fileList } = this.state;
        const uploadButton = (
            <div>
                <PlusOutlined />
                <div style={{ marginTop: 8 }}>Upload</div>
            </div>
        );
        const editData = {
            className: 'CS_edit',
            enterSend:false
        }
        var list = (length, url) => {
            var res = []
            for (let index = 0; index < length; index++) {
                res.push(url)
            }
            return res
        }
        const emojiData = {
            itemWidth: "2vw",
            itemHeight: "2vw",
            imgWidth: "80%",
            imgHeight: "auto",
            code:emojiList,
            url:(code)=>config.emojiUrl+code,
            clickEvent:this.emojiClickEvent
        }
        
        return (
            <div className='circleSend'>
                <Spin spinning={this.state.loading}>
                    <div className='CS_editBox'>
                        <Edit ref={c => this.Edit = c} {...editData} />
                        <div className='CS_es'>
                            <div className='CS_emoji'><CircleEmoji placement="top" {...emojiData} /></div>
                            <div className='CS_send' onClick={this.sendCircle}>发送</div>
                        </div>

                    </div>

                    <div className='CS_upload'>
                        <Upload
                            accept=".png, .jpg, .jpeg"
                            listType="picture-card"
                            fileList={fileList}
                            onChange={this.handleChange}
                            customRequest={this.customRequest}
                        >
                            {fileList.length >= 9 ? null : uploadButton}
                        </Upload>
                    </div>
                </Spin>
            </div>
        )
    }
}
export default connect(
    state=>({
        userData:state.userData
    })
    )(CircleSend)