import React, { Component } from 'react';
import { Scrollbars } from 'react-custom-scrollbars';
const scroll = {
    // 如果最终结果表格内容与表格头部无法对齐。
    // 表格头需要增加空白列，弹性布局
    width: '100%',
    // 最大高度，内容超出该高度会出现滚动条
    height: '100%',
}

export default function (MyComponent, style = scroll) {

    return class withScroll extends Component {
        scrollToButton = () => {
            console.log(this);
            // 一定要数据渲染完后，所有代码执行完后再触发滚动到底部，否则可能不会滚动到底部
            setTimeout(() => {
                this.scrollRef && this.scrollRef.scrollToBottom();
            });
        }
        onUpdate = () => {
            // 要使用防抖函数
            // scrollHeight（文档内容实际高度，包括超出视窗的溢出部分）、
            // scrollTop（滚动条滚动距离）、
            // clientHeight（窗口可视范围高度）。
            if (this.scrollRef.getScrollTop() < 30) {
                // console.log('<30');
                // 快滚动到头部
                //调用子组件的分页查询方法
                // console.log(this.scRef);
                this.myComponent && this.myComponent.scrollTop && this.myComponent.scrollTop()
            }
            if (this.scrollRef.getScrollHeight() - this.scrollRef.getScrollTop() - this.scrollRef.getClientHeight() < 30) {
                // 快滚动到底部
                //调用子组件的分页查询方法
                this.myComponent && this.myComponent.scrollButton && this.myComponent.scrollButton()
            }
        }
        render() {
            return (
                // {/* 父级一定要有高度才能自适应 */}
                <Scrollbars ref={c => this.scrollRef = c} style={style} onUpdate={this.onUpdate}>
                    <MyComponent scRef={c=>this.scRef = c} ref={c => this.myComponent = c} scrollToButton={this.scrollToButton} {...this.props} />
                </Scrollbars>
            );
        }
    }
}