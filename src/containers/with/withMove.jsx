import React, { Component } from 'react'
import { CloseCircleOutlined, HolderOutlined } from '@ant-design/icons'
import './withMove.scss'
export default function (MyComponent) {
    return class withMove extends Component {
        componentDidMount() {
            this.props.didMount()
        }
        componentWillUnmount() {
            this.props.willUnMount()
            // document.body.removeEventListener('click',this.bodyClick);
            // window.removeEventListener('mousemove', this.windowsMousemove)

        }
        
        // clientX 鼠标相对于浏览器左上角x轴的坐标； 不随滚动条滚动而改变；
        // clientY 鼠标相对于浏览器左上角y轴的坐标； 不随滚动条滚动而改变；
        // pageX 鼠标相对于浏览器左上角x轴的坐标； 随滚动条滚动而改变；
        // pageY 鼠标相对于浏览器左上角y轴的坐标； 随滚动条滚动而改变；
        // screenX 鼠标相对于显示器屏幕左上角x轴的坐标；
        // screenY 鼠标相对于显示器屏幕左上角y轴的坐标；
        // offsetX 鼠标相对于事件源左上角X轴的坐标；
        // offsetY 鼠标相对于事件源左上角Y轴的坐标；
        isDome = false
        startOffsetLeft = 0
        startOffsetTop = 0
        startMouseX = 0
        startMouseY = 0
        
        moveBoxClick = (e) => {
            e.stopPropagation()
            this.moveBox.style.zIndex = 99
            window.addEventListener('click',this.bodyClick)
            
        }
        mouseDome = (e) => {
            e.stopPropagation()
            this.isDome = true
            this.startMouseX = e.clientX
            this.startMouseY = e.clientY
            this.startOffsetLeft = this.moveBox.offsetLeft
            this.startOffsetTop = this.moveBox.offsetTop
            this.moveBox.style.cursor = "move"
            window.addEventListener('mousemove', this.windowsMousemove)
            window.addEventListener('mouseup',this.mouseup)
        }
        windowsMousemove = (e) => {
            if (this.isDome == false) {
                return;
            }
            let newMouseX = e.clientX
            let newMouseY = e.clientY
            this.moveBox.style.right = ''
            this.moveBox.style.bottom = ''
            this.moveBox.style.left = newMouseX - (this.startMouseX - this.startOffsetLeft) + 'px'
            this.moveBox.style.top = newMouseY - (this.startMouseY - this.startOffsetTop) + 'px'
        }
        mouseup = (e) => {
            this.isDome = false
            window.removeEventListener('mousemove', this.windowsMousemove)
            window.removeEventListener('mouseup',this.mouseup)
            this.moveBox.style.cursor = "default"
        }
        // 不要让技术牵着业务走
        render() {
            const { style } = this.props
            return (
                <div
                    ref={c => this.moveBox = c}
                    style={style}
                    onClick={this.moveBoxClick}
                >
                    <div
                        className='withMoveHead'
                    >
                        <HolderOutlined
                            // onClick={this.mouseup}
                            onMouseDown={this.mouseDome}
                            onMouseUp={this.mouseup} />
                        <CloseCircleOutlined onClick={this.props.close} />
                    </div>
                    < MyComponent {...this.props} />
                </div >
            )
        }
    }
}

