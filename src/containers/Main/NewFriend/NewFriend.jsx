import React, { Component } from 'react'
import axiosApi from '../../../api/axiosApi'
import './newFriend.scss'
import NewFriendMain from './NewFriendMain'
class NewFriend extends Component {
    render() {
        return (
            <div className='newFriend'>
                <div className='NF_header'>
                    新的盆友
                </div>
                <div className='NF_Main'>
                    <NewFriendMain />
                </div>

            </div>
        )
    }
}
export default NewFriend
