import React, { Component } from 'react'
import { Button } from 'antd'
import './newFriendMain.scss'
import withScroll from '../../with/withScroll'
import axiosApi from '../../../api/axiosApi'
import { connect } from 'react-redux'
import { addAddressBookItem } from '../../../redux/actions/addressList'
import {redNavUnreadMessages} from '../../../redux/actions/nav'
class NewFriendMain extends Component {
    pageData = {
        page: 1,
        size: 50
    }
    state = { newFriendList: [] }
    componentDidMount() {
        this.getNewFriendList()
        axiosApi.delFriendshipStatus().then(res=>{
            this.props.redNavUnreadMessages({redChangeName:"addressList",redChangeNum:100})
        })
    }
    getNewFriendList = () => {
        axiosApi.getNewFriendInfo(this.pageData).then(res => {
            console.log(res);
            this.setState({ newFriendList: res.data })
        })
    }
    upDataNewFriendList(newFriendList = [], friendId) {
        newFriendList.filter(item => {
            return item.friendId == friendId
        })[0].status = '已接受'
        console.log(newFriendList);
        this.setState({ newFriendList })
    }
    acceptFriend = (friendData) => {
        friendData.status = '已接受'
        let sendData = { ...friendData }
        delete sendData.userIcon
        axiosApi.updateFriendship(sendData).then(res => {
            this.upDataNewFriendList(this.state.newFriendList, friendData.friendId)
            this.props.addAddressBookItem({addName:'address',addData:friendData})
        })
    }
    render() {
        return (
            <div className='newFriendMain'>
                {
                    this.state.newFriendList.map((item) => {
                        return <div key={item.friendshipsId} className='NFM_Item'>
                            <div className='NFM_hp'>
                                <div>
                                    <img src={item.userIcon} alt="" />
                                </div>
                            </div>
                            <div className='NFM_main'>
                                <div className='NFM_userName'>{item.username}</div>
                                {/* <div className='NFM_userText'>456</div> */}
                            </div>
                            <div className='NFM_acc'>
                                {
                                    item.status == '等待接受' || item.status == '已读' ? <Button onClick={() => { this.acceptFriend(item) }}>接受</Button> : <span>{item.status}</span>
                                }
                            </div>
                        </div>
                    })
                }

            </div>
        )
    }
}
export default connect(
    state => ({

    }),
    {

        addAddressBookItem,
        redNavUnreadMessages
    }
)(withScroll(NewFriendMain))
