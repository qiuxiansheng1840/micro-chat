import React, { Component } from 'react';
import './groupDetail.scss'
import GroupDetailMain from './GroupDetailMain';
import { connect } from 'react-redux';
import { Button, Space } from 'antd';
import storage from '../../../util/storage';
import axiosApi from '../../../api/axiosApi';
import UUID from 'uuidjs'
import { delAddressBookItem } from '../../../redux/actions/addressList'
// 查找对应的群聊消息

class GroupDetail extends Component {
    findGroupDetail(groupId) {
        return this.props.addressList.group.list.filter((item) => {
            return item.groupId == groupId
        })[0]
    }
    delGroup = () => {
        const { groupId } = this.props.match.params
        axiosApi.delGroup({
            createTime: null,
            groupId,
            groupName: ""
        }).then(res => {
            // setTimeout(()=>{
            //     this.props.history.push(`/addressList`)
            // },0)
            this.props.history.push(`/addressList`)
            this.props.delAddressBookItem({ delName: 'group', delId: groupId })
            
            
        })
    }
    outGroup = () => {
        const myId = this.props.userData.id
        this.main.deleteGroupMember(myId)
    }
    checkGroupInChat = (groupId) => {
        return  this.props.messageList.some(item => {
            return item.groupId == groupId
        })
    }
    sendMessage = () => {
        const { groupId } = this.props.match.params
        const groupDetail = this.findGroupDetail(groupId)
        const sendData = {
            recentGroupTalkId: UUID.generate(),
            groupId,
            groupName: groupDetail.groupName,
            offlineGroupMsgNum: 0,
            userId: 1,
            groupMemberNum: 4,
            groupMsgList: [],
            draft: "",
            userList: []
        }
        if (this.checkGroupInChat(groupId)) {
            this.props.history.push(`/chatList/group/${groupId}`)
        } else {
            axiosApi.setRecentGroupTalk({
                groupId,
                recentGroupTalkId: 0,
                userId: this.props.userData.id
            }).then(res => {
                this.props.history.push(`/chatList/`)
            })
        }

        console.log(groupDetail);
        console.log(this.props);
    }
    render() {
        // console.log(this.props.userData);

        const myId = this.props.userData.id
        const { groupId } = this.props.match.params
        const data = this.findGroupDetail(groupId)
        return (
            <div className='groupDetail'>
                <div className='GD_header'>
                    {data.groupName}
                </div>
                <div className='GD_Main'>
                    <GroupDetailMain onRef={c => this.main = c} delGroup={this.delGroup} groupId={groupId} groupMemberList={[...data.groupMemberList]} />
                </div>
                <div className='GM_hand'>
                    <Space>

                        <Button onClick={this.sendMessage}>发消息</Button>
                        <Button onClick={this.outGroup}>退出群聊</Button>
                        {data.groupMemberList[0].id == myId ? <Button onClick={this.delGroup}>解散群聊</Button> : ''}
                    </Space>
                </div>
            </div>
        );
    }
}

export default connect(
    state => ({
        addressList: state.addressList,
        userData: state.userData,
        messageList: state.messageList
    }),
    {
        delAddressBookItem
    }

)(GroupDetail);