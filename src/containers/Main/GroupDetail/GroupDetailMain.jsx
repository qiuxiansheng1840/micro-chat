import React, { Component } from 'react'
import './groupDetailMain.scss'
import { Avatar, Image, message, Popover } from 'antd';
import { PlusSquareOutlined } from '@ant-design/icons';
import withScroll from '../../with/withScroll'
import { connect } from 'react-redux';
import { showRightClick } from '../../../redux/actions/rightClick'
import { delGroupMember, addGroupMember } from '../../../redux/actions/addressList'
import axiosApi from '../../../api/axiosApi'
import storage from '../../../util/storage';
import { Scrollbars } from 'react-custom-scrollbars';
import AddressListItem from '../../List/AddressList/AddressListItem/AddressListItem'
class GroupDetailMain extends Component {
    state = { showPopover: false }
    componentDidMount() {
        this.props.onRef(this)
    }
    GroupMemberId = null
    contextMenu = (e) => {
        e.preventDefault()
        //阻止事件冒泡，使点击范围在Item内时不会触发document的监听事件
        e.stopPropagation()
        this.props.showRightClick({
            isShow: true,
            x: e.clientX,
            y: e.clientY,
            handleFn: [
                {
                    name: '删除群友',
                    handle: this.deleteGroupMember
                }
            ]
        })
    }
    deleteGroupMember = (id) => {
        console.log(this.props);
        const { groupId } = this.props
        axiosApi.delGroupMember({
            createTime: null,
            groupId,
            groupUserId: null,
            updateTime: null,
            userId: this.GroupMemberId
        }).then(res => {
            if(this.props.groupMemberList.length==2){
                this.props.delGroup()
            }else{
                this.props.delGroupMember({ groupId: groupId, userId: this.GroupMemberId })
            }
        }).catch(err => {
            message.error('只有群主才能删除群成员')
        })
    }
    addGroupMember = (userDate) => {
        this.setState({ showPopover: false })
        const { groupId } = this.props
        let memberData = {
            id: userDate.friendId,
            username: userDate.username,
            icon: userDate.userIcon
        }
        axiosApi.addGroupUser({
            createTime: null,
            groupId,
            groupUserId: 0,
            updateTime: null,
            userId: userDate.friendId
        }).then(res => {
            this.props.addGroupMember({ groupId, memberData })
        })
    }
    onVisibleChange = (flag) => {
        this.setState({ showPopover: flag })
    }
    render() {
        const myId = this.props.userData.id
        const isOwner = this.props.groupMemberList[0].id == myId
        const notInGroupList = this.props.addressList.address.list.filter((item) => {
            for (let index = 0; index < this.props.groupMemberList.length; index++) {
                const element = this.props.groupMemberList[index];
                if (element.id == item.friendId) return false
            }
            return true
        })
        const content = (
            <div className='GDM_popoverContent'>
                <Scrollbars style={{ height: '100%', width: "100%" }}>
                    {
                        notInGroupList.map((item) => {
                            return <div key={item.friendId + 'f'} onClick={() => { this.addGroupMember(item) }}><AddressListItem friendShipData={item} /></div>
                        })
                    }
                </Scrollbars>
            </div>
        )
        return (
            <div className='groupDetailMain'>
                {
                    this.props.groupMemberList.map((item, index) => {
                        return <div onContextMenu={isOwner ? (e) => { this.contextMenu(e); this.GroupMemberId = item.id } : ''} key={item.id} className='GDM_item'>
                            <Avatar shape="square" src={item.icon} />
                            <br />
                            <span>{item.username}</span>
                            {index == 0 ? <span style={{ color: "#FC011A" }}>(群主)</span> : ''}
                        </div>
                    })
                }
                <Popover visible={this.state.showPopover} onVisibleChange={this.onVisibleChange} content={notInGroupList.length ? content : '所有好友已在群聊中'} trigger="click">
                    <div className='GDM_item' style={{ cursor: "pointer" }}>
                        <Avatar shape="square" icon={<PlusSquareOutlined />} />
                        <br />
                        <span>添加新成员</span>
                    </div>
                </Popover>
            </div>
        )
    }
}
export default connect(
    state => ({
        addressList: state.addressList,
        userData:state.userData
    }),
    {
        showRightClick,
        delGroupMember,
        addGroupMember
    }
)(withScroll(GroupDetailMain)) 