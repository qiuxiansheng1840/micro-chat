import React, { Component } from 'react'
import { Route, Switch, Redirect } from 'react-router-dom'
import Welcome from '../../components/Welcome/Welcome'
import Chat from './Chat/Chat'
import NewFriend from './NewFriend/NewFriend'
import UserDetail from './UserDetail/UserDetail'
import GroupDetail from './GroupDetail/GroupDetail'
import './main.scss'
export default class Main extends Component {
    render() {
        return (
            <div id='main'>
                <Switch>
                    <Route path='/chatList/group/:chatId' component={Chat} />
                    <Route path='/chatList/:chatId' component={Chat} />

                    <Route path='/chatList' component={Welcome} />
                    <Route path='/addressList/newFriend' component={NewFriend} />
                    <Route path='/addressList/group/:groupId' component={GroupDetail} />
                    <Route path='/addressList/:friendshipsId' component={UserDetail} />
                    <Route path='/addressList' component={Welcome} />
                </Switch>
            </div>
        )
    }
}
