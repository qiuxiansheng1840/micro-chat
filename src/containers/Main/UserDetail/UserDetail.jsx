import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Button } from 'antd'
import UUID from 'uuidjs'
import { ManOutlined, WomanOutlined } from '@ant-design/icons';
import { addChat } from '../../../redux/actions/messageList';
import './userDetail.scss'
import axiosApi from '../../../api/axiosApi';
import storage from '../../../util/storage';
class UserDetail extends Component {
    /**
     * 
     * @returns {
        userName: 'lili',
        id: 'uaa',
        icon: '',
        message: []
    }
     */
    checkInMessageList(receiveId) {
        return this.props.messageList.some((item) => {
            return item.receiveId == receiveId
        })
    }
    sendMessage = () => {
        const { friendshipsId } = this.props.match.params
        const userObj = this.getUserObj(friendshipsId)
        const {userData} = this.props
        console.log(userObj);

        let sendData = {
            recentId: UUID.generate(),
            userId: userData.id,
            username: userData.username, //从storage中拿
            userIcon: null,
            receiveId: userObj.friendId==userData.id?userObj.userId:userObj.friendId,
            receiveName: userObj.username,
            receiveIcon: userObj.userIcon,
            offlineMessageNum: 0,
            creatTime: Date.now(),
            messageList: [],
        }
        if (this.checkInMessageList(userObj.friendId==userData.id?userObj.userId:userObj.friendId)) {
            this.props.history.push(`/chatList/${userObj.friendId==userData.id?userObj.userId:userObj.friendId}`)
        } else {
            axiosApi.setRecentTalk({
                receiveId: userObj.friendId==userData.id?userObj.userId:userObj.friendId,
                recentId: null,
                userId: userData.id
            }).then(res => {
                this.props.addChat({ userId: sendData.receiveId, innerData: sendData })
                setTimeout(this.props.history.push(`/chatList/${sendData.receiveId}`), 1)

            })
        }

        // sendData.id 应该换成后台的真实id后在跳转



    }

    getUserObj = (friendshipsId) => {
        return this.props.addressList.address.list.find((item) => {
            return item.friendId == friendshipsId
        })
    }

    render() {
        const { friendshipsId } = this.props.match.params
        const userObj = this.getUserObj(friendshipsId)
        console.log(userObj);
        return (
            <div className='userDetail'>
                <div className='UD_header'>
                    <div className='UD_data'>
                        <div className='UD_userName'>
                            {userObj.username}
                            <span className='UD_userSex'> <ManOutlined /><WomanOutlined /></span>
                        </div>
                        <div className="UD_phone">
                            {userObj.phone}
                        </div>
                    </div>
                    <div className='UD_HP'>
                        <div>
                            <img src="/img/钢铁侠头像.jpeg" alt="" />
                        </div>
                    </div>

                </div>
                <div className='UD_handle'>
                    <Button onClick={this.sendMessage}>发消息</Button>
                </div>
            </div>
        )
    }
}
export default connect(
    state => ({
        addressList: state.addressList,
        messageList: state.messageList,
        userData:state.userData
    }),
    {
        addChat
    }
)(UserDetail)
