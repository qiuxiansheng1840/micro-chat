import React, { Component } from 'react';
import './ChatHeader.scss'

class ChatHeader extends Component {
    render() {
        let {showData} = this.props
        return (
            <div className='chatHeader'>
                {showData}
            </div>
        );
    }
}

export default ChatHeader;