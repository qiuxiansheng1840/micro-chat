import React, { Component } from 'react'
import { connect } from 'react-redux';
import ChatHeader from './header/ChatHeader';
import ChatMain from './ChatMain/ChatMain';
import Editor from './Editor/Editor';
import './chat.scss'
class Chat extends Component {
    findMessageList(chatId, isGroup) {
        const { messageList } = this.props
        if (isGroup) {
            return messageList.find((value) => {
                return value.groupId == chatId
            })
        } else {
            return messageList.find((value) => {
                return value.receiveId === chatId
            })
        }
    }
    
    render() {
        let isGroup = false
        if (this.props.location.pathname.indexOf('group') != -1) {
            isGroup = true
        }
        let { chatId } = this.props.match.params
        const showData = this.findMessageList(chatId, isGroup)
        console.log(showData);
        return (
            <div id='chat'>
                <div className="header">
                    <ChatHeader showData={isGroup ?  showData.groupName : showData.receiveName} />
                </div>
                <div className='main'>
                    <ChatMain chatId={chatId} isGroup={isGroup} showData={isGroup ? [...showData.groupMsgList] : [...showData.messageList]} />
                </div>
                <div className="editor">
                    <Editor showData={showData.draft} isGroup={isGroup} chatId={chatId} />
                </div>
            </div>
        )
    }
}
export default connect(
    state => ({
        messageList: state.messageList
    })
)(Chat)