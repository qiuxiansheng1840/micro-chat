import React, { Component } from 'react'
import { SmileOutlined, HeartOutlined } from '@ant-design/icons'
import { Tabs } from 'antd';
import PubSub from 'pubsub-js';
import Emoji from '../../../../../../components/Emoji/Emoji';
import './Emoticon.scss'
import axiosApi from '../../../../../../api/axiosApi';
import emojiList from '../../../../../../util/emojiList';
import config from '../../../../../../config';
import { message } from 'antd';
import storage from '../../../../../../util/storage';
import { connect } from 'react-redux';
const { TabPane } = Tabs;

class Emoticon extends Component {
    pageData = {
        page: 1,
        size: 100
    }
    // 表情包列表
    state = { emoticonList: [] }
    checkEmotionIsIn = (path) => {
        return this.state.emoticonList.filter((item) => {
            return item.emojiPath == path
        }).length
    }
    componentDidMount() {
        PubSub.unsubscribe('addEmotion') //销毁订阅 防止多次触发
        PubSub.subscribe('addEmotion', (_, emotionData) => {
            if (this.checkEmotionIsIn(emotionData.emojiPath)) {
                message.info('你已经拥有该表情')
            } else {
                axiosApi.saveEmoji_user({
                    emojiId: emotionData.emojiId,
                    emojiMapId: 0,
                    userId: this.props.userData.id
                }).then(res => {
                    this.setState({ emoticonList: [...this.state.emoticonList, emotionData] })
                })
            }
        })
        axiosApi.getEmoji_user(this.pageData).then(res => {
            if (res.data) {
                this.setState({ emoticonList: res.data })
            }

        })
    }
    componentWillUnmount() {
        PubSub.unsubscribe('addEmotion') //销毁订阅 
    }
    emojiClickEvent = (emojiCode) => {
        PubSub.publish('emojiClick', emojiCode)
        // PubSub.publish('hide',emojiCode)
    }
    emotionClickEvent = (emotion) => {
        PubSub.publish('sendMessage', { innerType: 'biaoqin', innerMsg: JSON.stringify(emotion) })
    }
    render() {
        const emojiData = {
            itemWidth: "2vw",
            itemHeight: "2vw",
            imgWidth: "80%",
            imgHeight: "auto",
            code: emojiList,
            url: (code) => config.emojiUrl + code,
            clickEvent: this.emojiClickEvent
        }
        console.log(this.state.emoticonList);
        const emoticonStyle = {
            itemWidth: "4vw",
            itemHeight: "4vw",
            imgMaxWidth: "80%",
            imgWidth: "80%",
            imgHeight: "auto",
            itemPadding: "5px",
            code: [...this.state.emoticonList],
            url: (item) => item.emojiPath,
            clickEvent: this.emotionClickEvent
        }
        return (
            <div className='emoticon'>
                <Tabs tabPosition='bottom' size="small">
                    <TabPane tab={<SmileOutlined />} key="1">
                        <div className='EmoticonItem'>
                            <Emoji {...emojiData} />
                        </div>
                    </TabPane>
                    <TabPane tab={<HeartOutlined />} key="2">
                        <div className='EmoticonItem'>
                            <Emoji {...emoticonStyle} />
                        </div>
                    </TabPane>
                </Tabs>
            </div>
        )
    }
}
export default connect(
    state => ({
        userData:state.userData
    }),
)(Emoticon)