import React, { Component } from 'react'
import { SmileOutlined, FileAddOutlined } from '@ant-design/icons'
import { connect } from 'react-redux'
import { Popover, Upload, Button } from 'antd'
import { withRouter } from 'react-router-dom/cjs/react-router-dom.min'
import Emoticon from './Emoticon/Emoticon'
import { sendMessage } from '../../../../../redux/actions/messageList'
import './EditorHeader.scss'
import axiosApi from '../../../../../api/axiosApi'
import storage from '../../../../../util/storage'
import sparkMd5 from 'spark-md5';
import util from '../../../../../util/util'
import UUID from 'uuidjs'
import PubSub from 'pubsub-js'
import { editorSend } from '../../../../../redux/actions/messageList'

class EditorHeader extends Component {
    fileList = []
    fileBeforeUpload = (file) => {
        let type = ''
        // 判断文件什么类型
        switch (file.type) {
            case 'image/jpeg':
            case 'image/jpg':
            case 'image/png':
                type = 'image';
                break
            case 'video/mp4':
                type = 'video'
                break
            default:
                type = 'file';
                break
        }
        util.getBase64(file).then(res => {
            console.log(file);
            let md5Code = sparkMd5.hash(res)
            const fileData = new FormData()
            const { isGroup } = this.props
            let { chatId } = this.props
            let myId = this.props.userData.id
            fileData.append('file', file)
            fileData.append('fileId', 0)
            fileData.append('fileKey', md5Code)
            fileData.append('fileName', file.name)
            fileData.append('fileType', type)
            fileData.append('fileSuffix', file.name.slice(file.name.lastIndexOf('.')))
            fileData.append('groupMsgId', isGroup ? chatId : 0)
            fileData.append('receiverId', isGroup ? 0 : chatId)
            fileData.append('senderId', myId)
            fileData.append('createTime', Date.now().toString())
            axiosApi.upDataFile(fileData).then(res => {
                let fileMsg = {
                    filePath: res.filePath,
                    fileName: file.name,
                    fileSize: util.filterSize(file.size)
                }
                // this.sendMessage(res.filePath, type)
                PubSub.publish('sendMessage', { innerType: type, innerMsg: JSON.stringify(fileMsg) })
            })
        })
        return Promise.reject()
    }
    sendMessage(url, type) {
        const { isGroup } = this.props
        let { chatId,userData } = this.props
        let sendData = null
        let messageId = UUID.generate()
        let myId = userData.id
        // 如果是群聊
        if (isGroup) {
            sendData = {
                groupMsgId: messageId,
                senderId: myId,
                username: userData.username,
                    icon: userData.icon,
                groupId: chatId,
                groupMsgText: url,
                groupMsgType: type,
                createTime: new Date().getTime().toString(),
            }
        } else {
            sendData = {
                messageId: messageId,
                sendId: myId,
                receiveId: chatId,
                text: url,
                sendTime: new Date().getTime().toString(),
                receiveTime: '',
                status: "消息未接收",
                type: type
            }
        }
        this.props.editorSend({ chatId, message: sendData, isGroup })
        PubSub.publish('scrollToButton')
    }
    render() {
        return (
            <div className='editorHeader'>
                <Popover placement="top"
                    content={<Emoticon />}
                    trigger="click"
                >
                    <SmileOutlined />
                </Popover>
                <Upload
                    beforeUpload={this.fileBeforeUpload}
                    fileList={this.fileList}
                    onChange={this.fileChange}
                >
                    <FileAddOutlined />
                </Upload>
            </div>
        )
    }
}
export default connect(
    state => ({
        userData: state.userData
    }), {
    sendMessage,
    editorSend
}

)(withRouter(EditorHeader))