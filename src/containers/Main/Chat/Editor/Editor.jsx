import React, { Component } from 'react';
import { Button } from 'antd';
import { SendOutlined } from '@ant-design/icons'
import './editor.scss'
import EditorHeader from './EditorHeader/EditorHeader';
import EditBox from './EditBox/EditBox';
class Editor extends Component {
    send=()=>{
        this.editBox.sendMessage()
    }
    render() {
        return (
            <div className='chatEditor'>
                <div className="editorHeader">
                    <EditorHeader {...this.props}/>
                </div>
                <div className='editorMain'>
                    {/* onRef 为了获取到高阶组件里的子组件 */}
                    <EditBox {...this.props} onRef={c => this.editBox = c} />
                </div>
                <div className='sendButton'>
                    <Button onClick={this.send} type="link" icon={<SendOutlined />}>
                        发送
                    </Button>
                </div>
            </div>
        );
    }
}

export default Editor;