import React, { Component } from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import { editorChange, editorSend, changeMessage } from '../../../../../redux/actions/messageList'
import PubSub from 'pubsub-js'
import UUID from 'uuidjs'
import './EditBox.scss'
import withScroll from '../../../../with/withScroll'
import util from '../../../../../util/util'
import Edit from '../../../../../components/Edit/Edit'
import storage from '../../../../../util/storage'
import axiosApi from '../../../../../api/axiosApi'
import config from '../../../../../config'
class EditBox extends Component {
    // 是否需要将草稿写入编辑框,用来判断是否是因为路由引发的更新
    showDraft = false
    UNLISTEN = null
    path = null
    componentDidMount() {
        const { history } = this.props
        console.log(history);
        this.path = history.location.pathname
        // UNLISTEN变量用来接收解绑函数
        this.UNLISTEN = history.listen((route) => {
            console.log(route);
            // if (route.pathname.indexOf(this.props.chatId) == -1 && route.pathname.indexOf('chatList') != -1) {
            if (route.pathname!= this.path) {
                this.path = route.pathname
                // 路由改变了
                console.log('路由改变了');
                this.showDraft = true
                this.routeChange()
            }
        })
        // 将redux的草稿渲染到组件中 
        this.DraftChange()
        // 让Editor能获取到我
        this.props.onRef && this.props.onRef(this)
        PubSub.unsubscribe('emojiClick', this.emojiClick) //销毁订阅 防止多次触发
        PubSub.subscribe('emojiClick', this.emojiClick)
        PubSub.unsubscribe('sendMessage', this.sendMessage) //销毁订阅 防止多次触发
        PubSub.subscribe('sendMessage', this.sendMessage)

    }
    emojiClick = (_, emojiCode) => {
        const emojiHtml = `<img style="vertical-align: sub;" src="${config.emojiUrl}${emojiCode}">`
        this.Edit.emojiClick(emojiHtml)
    }
    componentDidUpdate() {
        if (this.showDraft) {
            this.DraftChange()
        }
    }
    componentWillUnmount() {
        this.routeChange()
        this.UNLISTEN && this.UNLISTEN(); // 执行解绑
    }
    // 改变消息列表的草稿显示
    DraftChange() {
        this.showDraft = false
        // 此时获取到的props为最新父组件赋予的数据
        let { showData, chatId, isGroup } = this.props
        this.Edit.setInnerHTML(util.stringToHTML(showData))
        this.props.editorChange({ chatId, draft: '', isGroup })
    }
    // 路由改变时
    routeChange = () => {
        // 此时的chatId还是旧的，因为history.listen会比父组件更新prop快
        // 更新草稿
        let { chatId, isGroup } = this.props;
        let context = this.Edit.getInnerHTML() || '';
        context = util.HTMLToString(context)
        this.props.editorChange({ chatId, draft: context, isGroup })
    }
    // 发送消息
    sendMessage = (_, innerData = {}) => {
        const { innerType, innerMsg } = innerData
        console.log(innerMsg);
        if (innerMsg || util.HTMLIsNull(this.Edit.getInnerHTML())) {
            let message = util.HTMLToString(this.Edit.getInnerHTML())
            const { isGroup, userData } = this.props
            let { chatId } = this.props
            let sendData = null
            let messageId = UUID.generate()
            let myId = userData.id
            // 如果是群聊
            if (isGroup) {
                sendData = {
                    groupMsgId: messageId,
                    senderId: myId,
                    username: userData.username,
                    icon: userData.icon,
                    groupId: chatId,
                    groupMsgText: innerMsg || message,
                    groupMsgType: innerType || "text",
                    createTime: new Date().getTime().toString(),
                }
            } else {
                sendData = {
                    messageId: messageId,
                    sendId: myId,
                    receiveId: chatId,
                    text: innerMsg || message,
                    sendTime: new Date().getTime().toString(),
                    receiveTime: '',
                    status: "消息未接收",
                    type: innerType || "text"
                }
            }
            this.props.editorSend({ chatId, message: sendData, isGroup })
            PubSub.publish('scrollToButton')
            if (isGroup) {
                axiosApi.sendGroupMsg({
                    groupMsgId: 0,
                    senderId: myId,
                    groupId: chatId,
                    groupMsgText: innerMsg || message,
                    groupMsgType: innerType || "text",
                    createTime: null,
                }).then(res => {
                    this.props.changeMessage({ chatId, oldMessageId: messageId, newMessage: { groupMsgId: res.groupMsgId }, isGroup })
                })
            } else {
                axiosApi.sendMessage(
                    {
                        messageId: null,
                        sendId: myId,
                        receiveId: chatId,
                        text: innerMsg || message,
                        sendTime: null,
                        receiveTime: null,
                        status: "消息未接收",
                        type: innerType || "text"
                    }
                ).then(res => {
                    this.props.changeMessage({ chatId, oldMessageId: messageId, newMessage: { messageId: res.messageId }, isGroup })
                })
            }

        }
        this.Edit.clear()
    }
    render() {
        const editData = {
            className: "inputBox",
            send: this.sendMessage
        }
        return (
            <div className='editBox'>
                <Edit ref={c => this.Edit = c} {...editData} />
            </div>
        )
    }
}

export default connect(
    state => ({
        messageList: state.messageList,
        userData: state.userData
    }),
    {
        editorChange,
        editorSend,
        changeMessage
    }
)(withScroll(withRouter(EditBox))) 