import React, { Component } from 'react'
import './timeMessage.scss'
export default class TimeMessage extends Component {
    render() {
        return (
            <div className='timeMessage'>
                <span>
                   {this.props.showDate}
                </span>
            </div>
        )
    }
}
