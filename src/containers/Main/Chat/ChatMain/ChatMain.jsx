import React, { Component, Fragment } from 'react'
import MessageItem from './MessageItem/MessageItem'
import TimeMessage from './TimeMessage/TimeMessage'
import { Image } from 'antd'
import { connect } from 'react-redux'
import PubSub from 'pubsub-js'
import util from '../../../../util/util'
import withScroll from '../../../with/withScroll'
import './chatMain.scss'
import axiosApi from '../../../../api/axiosApi'
import { withRouter } from 'react-router-dom'

class ChatMain extends Component {
    UNLISTEN = null
    componentDidMount() {
        this.scRef = this
        PubSub.unsubscribe('scrollToButton') //销毁订阅 防止多次触发
        PubSub.subscribe('scrollToButton', this.props.scrollToButton)
        const { history } = this.props
        this.UNLISTEN = history.listen((route) => {

            this.flag = true
        })
    }
    componentWillUnmount() {
        this.UNLISTEN && this.UNLISTEN(); // 执行解绑
    }
    componentDidUpdate() {
        const { isGroup, chatId } = this.props
        if (isGroup) {
            axiosApi.updateUnreadGroupMessage({
                groupId: chatId
            })
        } else {
            axiosApi.updateUnreadMessage({
                receiveId: chatId
            })
        }
        console.log(this.props.chatId);
    }
    flag = true
    // 向上查找历史记录
    scrollTop = () => {
        if (this.flag) {
            this.flag = false
            const { showData, isGroup, chatId } = this.props
            if (isGroup) {
                console.log(showData);
                // axiosApi.getMoreGroupMessage({
                //     groupId:chatId,
                //     startId:""
                // })
            } else {

            }

            console.log(showData);
        }

    }

    render() {
        setTimeout(() => {
            this.props.scrollToButton();
        }, 100)
        const { showData, isGroup } = this.props
        let previousDate = 0;
        let nextDate = 0;
        console.log(this.props);
        return (
            <div className='chatMain'>
                <Image.PreviewGroup>
                    {
                        showData.map((item) => {
                            nextDate = isGroup ? item.createTime : item.sendTime
                            let showDate = util.CalculationTimeDifferenceForChat(previousDate, nextDate)
                            previousDate = nextDate
                            return (
                                <Fragment key={isGroup ? item.groupMsgId + 'GM' : item.messageId} >
                                    {showDate ? <TimeMessage showDate={showDate} /> : ''}
                                    <MessageItem isGroup={isGroup} {...item} />
                                </Fragment>

                            )
                        })
                    }
                </Image.PreviewGroup>
            </div>
        )
    }
}
export default connect(
    state => ({
        messageList: state.messageList,
        addressList: state.addressList
    })
)(withScroll(withRouter(ChatMain))) 