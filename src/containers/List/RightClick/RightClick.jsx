import React, { Component } from 'react'
import { connect } from 'react-redux';
import { closeRightClick } from '../../../redux/actions/rightClick';
import './RightClick.scss'
// 修改右键菜单
class RightClick extends Component {
    componentDidMount() {
        const { documentEventHandle } = this
        document.addEventListener('contextmenu', documentEventHandle);
        document.addEventListener('click', documentEventHandle);
        document.addEventListener('mousewheel', documentEventHandle);
        // 兼容火狐浏览器
        document.addEventListener('DOMMouseScroll', documentEventHandle);
    }
    componentWillUnmount(){
        const { documentEventHandle } = this
        document.removeEventListener('contextmenu', documentEventHandle);
        document.removeEventListener('click', documentEventHandle);
        document.removeEventListener('mousewheel', documentEventHandle);
        // 兼容火狐浏览器
        document.removeEventListener('DOMMouseScroll', documentEventHandle);
    }
    documentEventHandle = () => {
        if (this.props.showData.isShow) this.props.closeRightClick()
    }
    render() {
        let { handleFn=[], isShow, x, y } = this.props.showData
        return (
            <div style={{ display: isShow ? 'block' : 'none', top: y, left: x }} ref={c => this.rightClick = c} className='right-click'>
                {
                     handleFn.map((item, index) => {
                        return <div className='click-item' key={index} onClick={item.handle}>{item.name}</div>
                     })
                 } 
            </div>
        );
    }
}

export default connect(
    state => ({
        showData: state.rightClick
    }),
    {
        closeRightClick
    }
)(RightClick);
