import React, { Component } from 'react'
import { connect } from 'react-redux'
import { NavLink } from 'react-router-dom'
import ChatListitem from './ChatListitem/ChatListitem'
import ChatListGroup from './ChatListitem/ChatListGroup'
import withScroll from '../../with/withScroll'
import { initMessageList } from '../../../redux/actions/messageList'
import axiosApi from '../../../api/axiosApi'
function sortMessageList(list) {
    list.sort((previous, next) => {
        let previousTime = getGroupOrMessageTime(previous)
        let nextTime = getGroupOrMessageTime(next)
        if (previousTime < nextTime) return 1
        return -1
    })
    return list
}
// 获取群聊或单聊的最近消息的创建时间
function getGroupOrMessageTime(list) {
    return (list.groupMsgList && list.groupMsgList.length && list.groupMsgList[list.groupMsgList.length - 1].createTime) || (list.messageList && list.messageList.length && list.messageList[list.messageList.length - 1].sendTime)
}
class ChatList extends Component {
    componentDidMount() {
        axiosApi.getChatList().then(res => {
            console.log(res);
            if (res.length) {
                let messageList = res[0].messageList || (res[1] && res[1].messageList) ||[]
                let groupMsgList = res[0].groupMsgList || (res[1] && res[1].groupMsgList) ||[]
                messageList.reverse()
                groupMsgList.reverse()
                console.log(res);
            }
            this.props.initMessageList({ data: sortMessageList(res) })
        })
    }


    render() {
        const { messageList } = this.props
        return (
            <div id='ChatList'>
                {
                    messageList.map((item) => {
                        //要使用高亮效果，所以需要用params传参
                        return item.messageList ?
                            // 单聊
                            <NavLink key={item.receiveId} to={`/chatList/${item.receiveId}`}><ChatListitem key={item.receiveId} showData={{ ...item }} /></NavLink> :
                            //  群聊
                            <NavLink key={item.groupId + 'G'} to={`/chatList/group/${item.groupId}`}><ChatListGroup key={item.groupId} showData={{ ...item }} /></NavLink>

                    })
                }
            </div>
        )
    }
}
export default connect(
    state => ({
        messageList: state.messageList
    }),
    {
        initMessageList
    }
)(withScroll(ChatList)) 