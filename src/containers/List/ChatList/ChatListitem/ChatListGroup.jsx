import React, { Component } from 'react'
import './ChatListitem.scss'
import { connect } from 'react-redux'
import util from '../../../../util/util'
import { Badge, Avatar, Image } from 'antd'
import axiosApi from '../../../../api/axiosApi'
import { changeUnRead } from '../../../../redux/actions/messageList'
import { redNavUnreadMessages } from '../../../../redux/actions/nav'
import GroupHeadPortrait from '../../../../components/GroupHeadPortrait/GroupHeadPortrait'
class ChatListGroup extends Component {
    // 当点击了item时
    click = () => {
        this.props.changeUnRead({ chatId: this.props.showData.groupId, changeData: 0,isGroup:true })
        this.props.redNavUnreadMessages({ redChangeName: 'chat', redChangeNum: this.props.showData.offlineGroupMsgNum })
    }
    render() {
        const { showData } = this.props
        // 群聊的数据
        const { groupId, groupMemberNum, groupMsgList, groupName, recentId, messageList, offlineGroupMsgNum, recentGroupTalkId, userList, draft, createTime } = showData
        // 最后一条消息
        const lastGroupMsg = showData.groupMsgList &&  (showData.groupMsgList.length ? showData.groupMsgList[showData.groupMsgList.length - 1] : null)
        console.log(lastGroupMsg);
        return (
            <div className='chatListItem' onClick={this.click}>
                <div className='head-portrait'>
                    <Badge title='' size="small" count={offlineGroupMsgNum}>
                        <GroupHeadPortrait groupMemberList={userList} />
                    </Badge>
                </div>
                <div className='text'>
                    <div className='nameAndTime'>
                        <span className='name'>{groupName}</span>
                        {lastGroupMsg ?<span>{util.CalculationTimeDifferenceForCircle(lastGroupMsg.createTime)}</span>:""}
                        {/* <span>{lastGroupMsg ? util.CalculationTimeDifferenceForCircle(lastGroupMsg.createTime) : util.CalculationTimeDifferenceForCircle(createTime)}</span> */}
                    </div>
                    <div>
                        {
                            draft ?
                                <><span className='draft'>[草稿]</span><span dangerouslySetInnerHTML={{ __html: util.stringToHTML(draft) }}></span></> :
                                (() => {
                                    if (lastGroupMsg) {
                                        const {groupMsgType} = lastGroupMsg
                                        switch (groupMsgType) {
                                            case 'text':
                                                return <span className='main' dangerouslySetInnerHTML={{ __html: util.stringToHTML(lastGroupMsg.groupMsgText) }}></span>
                                            case 'image':
                                                return <span className='main'>[图片]</span>
                                            case 'video':
                                                return <span className='main'>[视频]</span>
                                            case 'file':
                                                return <span className='main'>[文件]</span>
                                            case 'biaoqin':
                                                return <span className='main'>[表情包]</span>
                                            default:
                                                return null
                                        }
                                    }
                                })()
                        }
                    </div>
                </div>
            </div>
        )
    }
}
export default connect(state => ({}),
    {
        changeUnRead,
        redNavUnreadMessages
    })(ChatListGroup)