import React, { Component } from 'react'
import { connect } from 'react-redux'
import './ChatListitem.scss'
import util from '../../../../util/util'
import { Badge, Avatar, Image } from 'antd'
import axiosApi from '../../../../api/axiosApi'
import {changeUnRead} from '../../../../redux/actions/messageList'
import {redNavUnreadMessages} from '../../../../redux/actions/nav'
class ChatListitem extends Component {
    // 当点击了item时
    click=()=>{
        this.props.changeUnRead({chatId:this.props.showData.receiveId,changeData:0})
        this.props.redNavUnreadMessages({redChangeName:'chat',redChangeNum:this.props.showData.offlineMessageNum})
    }
    render() {
        const { showData } = this.props
        console.log(showData);
        // 单聊的数据
        const { offlineMessageNum, receiveName, receiveIcon, receiveId, recentId, messageList, draft, createTime } = showData
        // 最后单聊一条消息
        const lastMessage = showData.messageList && (showData.messageList.length ? showData.messageList[showData.messageList.length - 1] : null)
        // 群聊 groupMsgList
        const lastGroupMsgList =  showData.groupMsgList && (showData.groupMsgList.length ? showData.groupMsgList[showData.groupMsgList.length - 1] : null)
        console.log('----->',lastMessage,lastGroupMsgList,showData);
        return (
            <div className='chatListItem' onClick={this.click}>
                <div className='head-portrait'>
                    <Badge title='' size="small" count={offlineMessageNum}>
                        <Avatar size="large" shape="square" src={<Image src={receiveIcon} />} />
                    </Badge>
                </div>
                <div className='text'>
                    <div className='nameAndTime'>
                        <span className='name'>{receiveName}</span>
                        {
                            lastMessage ? <span>{util.CalculationTimeDifferenceForCircle(lastMessage.sendTime)}</span>:""
                        }
                        {
                            lastGroupMsgList ?<span>{util.CalculationTimeDifferenceForCircle(lastGroupMsgList.sendTime)}</span>:""
                        }
                        
                    </div>
                    <div>
                        {
                            draft ?
                                <><span className='draft'>[草稿]</span><span dangerouslySetInnerHTML={{ __html: util.stringToHTML(draft) }}></span></> :
                                (() => {
                                    if (lastMessage) {
                                        switch (lastMessage.type) {
                                            case 'text':
                                                return <span className='main' dangerouslySetInnerHTML={{ __html: util.stringToHTML(lastMessage.text) }}></span>
                                            case 'image':
                                                return <span className='main'>[图片]</span>
                                            case 'video':
                                                return <span className='main'>[视频]</span>
                                            case 'file':
                                                return <span className='main'>[文件]</span>
                                            case 'biaoqin':
                                                return <span className='main'>[表情包]</span>
                                            default:
                                                return null
                                        }
                                    }
                                })()
                        }
                    </div>
                </div>
            </div>
        )
    }
}
export default connect(
    state=>({}),
    {
        changeUnRead,
        redNavUnreadMessages
    }
)(ChatListitem)