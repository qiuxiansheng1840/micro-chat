import React, { Component } from 'react'
import { connect } from 'react-redux'
import axiosApi from '../../../../api/axiosApi'
import GroupHeadPortrait from '../../../../components/GroupHeadPortrait/GroupHeadPortrait'
import { showRightClick } from '../../../../redux/actions/rightClick'
import { delAddressBookItem } from '../../../../redux/actions/addressList'
import util from '../../../../util/util'
import './AddressListItem.scss'
import storage from '../../../../util/storage'
import Item from 'antd/lib/list/Item'
import { withRouter } from 'react-router-dom'
class AddressListItem extends Component {
    contextMenu = (e) => {
        e.preventDefault()
        //阻止事件冒泡，使点击范围在Item内时不会触发document的监听事件
        e.stopPropagation()
        this.props.showRightClick({
            isShow: true,
            x: e.clientX,
            y: e.clientY,
            handleFn: [
                {
                    name: '删除好友',
                    handle: this.deleteFriend
                }
            ]
        })
    }
    findRecentTalkId = (id) => {
        return this.props.messageList.filter(item => {
            return item.receiveId == id
        })

    }
    deleteFriend = () => {
        const { friendshipsId, friendId, userId } = this.props.friendShipData
        const myId = this.props.userData.id
        axiosApi.deleteFriend({
            "createTime": null,
            "friendId": friendId == myId ? friendId : userId,
            "friendshipsId": friendshipsId,
            "status": null,
            "updateTime": null,
            "userId": myId
        }).then(res => {
            const findList = this.findRecentTalkId(friendId == myId ? friendId : userId)
            if (findList.length) {
                axiosApi.delMessageItem(findList[0].recentId).then(res => {
                    
                })
            }
            this.props.history.push(`/addressList`)
            this.props.delAddressBookItem({ delName: 'address', delId: friendshipsId })
            

        })
    }
    getRecentTalkId() {

    }
    render() {
        const { username, groupName, groupMemberList, userIcon } = this.props.friendShipData
        return (
            <div className='AddressListItem' onContextMenu={groupName ? () => { } : this.contextMenu}>
                <div className='head-portrait'>
                    {
                        groupName ?
                            <GroupHeadPortrait groupMemberList={[...groupMemberList]}></GroupHeadPortrait> :
                            <img src={userIcon} alt="" />
                    }
                </div>
                <div className='name'>
                    {username || groupName}
                </div>
            </div>
        )
    }
}
export default connect(
    state => ({
        showData: state.rightClick,
        messageList: state.messageList,
        userData: state.userData
    }),
    {
        showRightClick,
        delAddressBookItem
    }

)(withRouter(AddressListItem))