import React, { Component, Fragment } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import { Button } from 'antd'
import util from '../../../util/util'
import textHandle from '../../../util/textHandle'
import AddressListItem from './AddressListItem/AddressListItem'
import './AddressList.scss'
import withScroll from '../../with/withScroll.jsx'
import axiosApi from '../../../api/axiosApi'
import { initAddressList, addAddressBookItem } from '../../../redux/actions/addressList'
class AddressList extends Component {
    pageOV = {
        page: 1,
        size: 20
    }
    state = { showList: 'address' }
    componentDidMount() {
        axiosApi.getFriendList(this.pageOV).then(res => {
            this.props.initAddressList({ initName: 'address', initData: res })
        })
    }
    // scrollButton=()=>{
    //     this.pageOV.page++
    //     if(this.state.showList=='address'){
    //         axiosApi.getFriendList(this.pageOV).then(res=>{
    //             this.props.addAddressBookItem({addName:'address',addData:res})
    //         })
    //     }else{
    //         axiosApi.getGroupList(this.pageOV).then(res=>{
    //             this.props.addAddressBookItem({addName:'group',addData:res})
    //         })
    //     }
    // }


    // 将每个通讯录的item进行检查，拿取首字母
    CheckLetter = (item = '') => {
        let firstLetter = item.slice(0, 1);
        if (textHandle.isChinese(firstLetter)) {
            return textHandle.getPinYinFirstCharacter(firstLetter, '', true)
        }
        if (textHandle.isEnglish(firstLetter)) {
            return firstLetter.toUpperCase()
        }
        return '#'
    }
    // 判断是否需要更换展示字母
    changeLetter = (showLetter, newLetter) => {
        if (!showLetter || showLetter != newLetter) {
            return true
        }
        return false
    }
    changeShowList = () => {
        if (this.state.showList == 'address') {
            this.setState({ showList: 'group' })
            axiosApi.getGroupList(this.pageOV).then(res => {
                this.props.initAddressList({ initName: 'group', initData: res })
            })
        } else {
            this.setState({ showList: 'address' })
            axiosApi.getFriendList(this.pageOV).then(res => {
                this.props.initAddressList({ initName: 'address', initData: res })
            })
        }
    }
    render() {
        let addressList = util.addressListSort(this.props.addressList[this.state.showList].list, { address: 'username', group: 'groupName' }[this.state.showList])
        let showLetter = ''
        return (
            <div id='addressList'>
                <div className='toGroupList'>
                    <Button block onClick={this.changeShowList}>{this.state.showList == 'address' ? "我的群聊" : '我的好友'}</Button>
                </div>
                <div className='newFriend'>
                    <Link to="/addressList/newFriend"><Button block >新的盆友</Button></Link>
                </div>
                <div className='letter'>
                    {
                        
                        addressList.map((item) => {
                            // 首字母
                            let firstLetter = textHandle.getFirstLetter(item.username || item.groupName)
                            if (this.changeLetter(showLetter, firstLetter)) {
                                showLetter = firstLetter
                                return (
                                    <Fragment key={item.friendId || item.groupId}>
                                        <div className='letter'>{firstLetter}</div>
                                        <Link key={item.friendId || item.groupId} to={'/addressList/'+(item.friendId?'':'group/') + (item.friendId || item.groupId)}><AddressListItem friendShipData={{...item}} /></Link>
                                    </Fragment>
                                )
                            }
                            return <Link key={item.friendId || item.groupId} to={'/addressList/'+(item.friendId?'':'group/') + (item.friendId || item.groupId)}><AddressListItem friendShipData={{...item}} /></Link>
                        })
                    }
                </div>
            </div>
        )
    }
}
export default connect(
    state => ({
        addressList: state.addressList
    }),
    {
        initAddressList
    }
)(withScroll(AddressList)) 