import React, { Component } from 'react'
import { Input, Popover } from 'antd';
import AddFriend from './AddFriend/AddFriend';
import AddGroup from './AddGroup/AddGroup';
import { SearchOutlined, UserAddOutlined, UsergroupAddOutlined } from '@ant-design/icons'
import './ListHeader.scss'
export default class ListHeader extends Component {
    render() {
        return (
            // 再添加一个div用来占位
            <div id='listHeader'>
                <div className='search'>
                    <Input placeholder="搜索" prefix={<SearchOutlined />} />
                </div>
                <div className='add' title='添加好友'>
                    <Popover placement="bottom" content={<AddFriend/>} trigger="click">
                        <UserAddOutlined />
                    </Popover>

                </div>
                <div className='add' title='新建群聊'>
                    <Popover placement="bottom" content={<AddGroup/>} trigger="click">
                        <UsergroupAddOutlined />
                    </Popover>

                </div>
            </div>
        )
    }
}
