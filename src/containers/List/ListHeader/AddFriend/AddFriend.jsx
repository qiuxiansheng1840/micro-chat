import React, { Component } from 'react'
import { Input, Spin, message, Button } from 'antd'
import { AudioOutlined } from '@ant-design/icons';
import './addFriend.scss'
import axiosApi from '../../../../api/axiosApi';
import { connect } from 'react-redux';
const { Search } = Input;
class AddFriend extends Component {
    state = { isSearchImg: false, searched: false, findData: null }
    isMyFriend=(id)=>{
        return this.props.addressList.address.list.filter(item=>{
            return item.friendId == id
        }).length
    }
    onSearch = (value) => {
        const realValue = value.replace(/(^\s*)|(\s*$)/g, "")
        if (realValue) {
            this.setState({ isSearchImg: true, searched: false })
            axiosApi.findNewFriend(value).then(res => {
                this.setState({ isSearchImg: false, searched: true, findData: res })
                console.log(res);
            })
        } else {
            message.info('未输入内容')
        }
    }
    addFriend=()=>{
        const { findData } = this.state
        const {userData} = this.props
        if(findData.id==this.props.userData.id){
            message.info('不能添加自己为好友')
        }else if(this.isMyFriend(findData.id)){
            message.info('对方已经是你好友')
        }else {
            axiosApi.addFriend({
                createTime: null,
                friendId: findData.id,
                friendshipsId: 0,
                status: "等待接受",
                updateTime: null,
                userId: userData.id
            }).then(res=>{
                message.info('已添加')
                console.log(res);
            })
        }
        console.log(this.state.findData);
    }
    
    render() {
        const { findData } = this.state
        return (
            <div className='addFriend'>
                <Search placeholder="输入用户电话号码" onSearch={this.onSearch} style={{ width: "100%" }} />
                {this.state.isSearchImg ?
                    <div style={{ textAlign: "center", margin: '1vh' }}><Spin /></div> :
                    ""}
                {
                    this.state.searched ?
                        findData ?
                            <div className='AF_newFriend'>
                                <dir className="AF_HP" >
                                    <div>
                                        <img src={findData.icon} alt="" />
                                    </div>

                                </dir>
                                <div className='AF_userName'>
                                    {findData.username}
                                </div>
                                <div className='AF_addButton'>
                                    <Button size='small' ghost type="primary" onClick={this.addFriend}>添加</Button>
                                </div>
                            </div> : '未查找到新用户'
                        : ''
                }

            </div>
        )
    }
}

export default connect(
    state=>({
        addressList:state.addressList,
        userData:state.userData
    })
)(AddFriend)