import React, { Component } from 'react'
import { connect } from 'react-redux';
import { Checkbox, Button, message, Popover, Input } from 'antd';
import withScroll from '../../../with/withScroll';
import './addGroup.scss'
import axiosApi from '../../../../api/axiosApi';
import axios from 'axios';
const { Search } = Input
class AddGroup extends Component {
    checkedValues = []
    state = { visible: false }
    onChange = (checkedValues) => {
        this.checkedValues = checkedValues
    }
    creatGroup = () => {
        if (this.checkedValues.length > 1) {
            this.setState({ visible: true })
        } else if (this.checkedValues.length == 1) {
            message.info('选择人数不得小于2')
        } else {
            message.info('未选择人员')
        }

    }
    onVisibleChange = (value) => {
        if (!value) {
            this.setState({ visible: false })
        }
    }
    onSearch = (value) => {
        if (!value.trim().length) {
            message.info('未输入群名')
            return
        }
        const addGGroupRequestList = []
        axiosApi.addGroup({
            createTime: null,
            groupId: null,
            groupName: value.trim()
        }).then(res => {
            const {groupId} = res
            console.log(res);
            this.checkedValues.forEach(item => {
                addGGroupRequestList.push(
                    axiosApi.addGroupUser({
                    createTime: null,
                    groupId,
                    groupUserId: 0,
                    updateTime: "",
                    userId: item
                }))
            })
            axios.all(addGGroupRequestList).then(res=>{
                message.info('创建成功')
                console.log(res);
            })
            
        })
        console.log(this.checkedValues);
        console.log(value);
    }
    render() {
        const content = (<Search
            placeholder="输入群聊名称"
            enterButton="创建"
            size="middle"
            // suffix={suffix}
            onSearch={this.onSearch}
        />)
        const { userData } = this.props
        return (
            <div className='addGroup'>
                <div className='addBut'>
                    <Popover trigger='click' content={content} visible={this.state.visible} onVisibleChange={this.onVisibleChange}>
                        <Button ghost type="primary" onClick={this.creatGroup} >
                            创建群聊
                        </Button>
                    </Popover>

                </div>
                <Checkbox.Group style={{ width: '100%' }} onChange={this.onChange}>
                    {
                        this.props.addressList.address.list.map((item) => {
                            return <Checkbox key={item.friendshipsId} value={item.friendId == userData.id ? item.userId : item.friendId}>
                                <div className='AddressListItem'>
                                    <div className='head-portrait'>
                                        <img src={item.userIcon} alt="" />
                                    </div>
                                    <div className='name'>
                                        {item.username}
                                    </div>
                                </div>
                            </Checkbox>
                        })
                    }
                </Checkbox.Group>
            </div>
        )
    }
}
const scrollStyle = { width: '20vw', height: "50vh" }
export default connect(
    state => ({
        addressList: state.addressList,
        userData: state.userData
    })
)(withScroll(AddGroup, scrollStyle)) 