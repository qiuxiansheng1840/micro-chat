import React, { Component } from 'react'
import { Route, Switch,Redirect } from 'react-router-dom'
import ListHeader from './ListHeader/ListHeader'
import ChatList from './ChatList/ChatList'
import AddressList from './AddressList/AddressList'
import RightClick from './RightClick/RightClick'
import './list.scss'
export default class List extends Component {
    render() {
        return (
            <div id='list'>
                <div className='header'>
                    <ListHeader></ListHeader>
                </div>
                <div className='itemList'>
                    <Switch>
                        <Route path='/chatList' component={ChatList} />
                        <Route path='/addressList' component={AddressList} />
                        <Redirect to='/chatList'/>
                    </Switch>
                    <RightClick />

                    {/* <ChatList/> */}
                    {/* <AddressList/> */}
                </div>


            </div>
        )
    }
}
