import React, { Component } from 'react'
import { Form, Input, Button, Radio, Checkbox, Upload, message } from 'antd';
import { ChromeOutlined } from '@ant-design/icons'
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import './login.scss'
import { initUserData } from '../../redux/actions/userData';
import axiosApi from '../../api/axiosApi';


class Login extends Component {
    onFinish = (values) => {
        axiosApi.login(values)
            .then(res => {
                res.token = res.password
                this.props.initUserData(res)
                this.props.history.push(`/`)
                console.log(res);
            }).catch(err => {
                message.error('用户名或密码有误')
            })
    }
    render() {
        return (
            <div className='login'>

                <div className="from">
                    <div className='logo'>
                        <span>微聊天</span>
                        <ChromeOutlined style={{ marginLeft: '-3vw', color: "#97AEC5" }} spin />
                    </div>
                    <Form
                        ref={c => this.form = c}
                        name="basic"
                        labelCol={{ span: 5 }}
                        wrapperCol={{ span: 20 }}
                        onFinish={this.onFinish}
                        autoComplete="off"
                        labelAlign="right"
                    >
                        <Form.Item
                            label="用户名"
                            name="username"
                            rules={[{ required: true, message: '请输入用户名' }]}
                        >
                            <Input />
                        </Form.Item>

                        <Form.Item
                            label="密码"
                            name="password"
                            rules={[{ required: true, message: '请输入密码' }]}
                        >
                            <Input.Password />
                        </Form.Item>

                        <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
                            <Button ghost style={{ margin: '0 8px' }} htmlType="submit" type="primary">
                                登录
                            </Button>
                            <Link to="/signin">
                                <Button ghost type="primary">
                                    注册
                                </Button>
                            </Link>



                        </Form.Item>
                    </Form>
                </div>
            </div>
        )
    }
}
export default connect(
    state => ({}),
    {
        initUserData
    }
)(Login)