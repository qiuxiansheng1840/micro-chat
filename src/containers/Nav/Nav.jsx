import React, { Component } from 'react'
import { SolutionOutlined, MessageOutlined, DeploymentUnitOutlined } from '@ant-design/icons'
import { Badge, Avatar } from 'antd';
import { NavLink, withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import { initNavUnreadMessages } from '../../redux/actions/nav';
import axiosApi from '../../api/axiosApi';
import socketFn from '../../api/socketApi';
import init from '../../api/initApi'
import './nav.scss'
import CircleOfFriends from '../CircleOfFriends/CircleOfFriends';

class Nav extends Component {
    componentDidMount() {
        init()
        axiosApi.getOfflineMessageNum().then(res => {
            const initState = {
                chat: res[0],
                addressList: res[1],
                square: res[2]
            }
            this.props.initNavUnreadMessages(initState)
            try {
                socketFn().connection();
                console.log('Nav 测试连接');
            } catch (e) {
                // 捕获异常，防止js error
                // donothing
            }
        })

    }
    
    state = { showCircle: false, circleZIndex: 99 }
    spanTarget = ''
    charHref = '/chatList'
    addressHref = '/addressList'
    showCircle = (e) => {
        this.spanTarget = e.target
        this.setState({ showCircle: true })
    }
    closeCircle = () => {
        this.setState({ showCircle: false })
    }
    circleDidMount = () => {
        window.addEventListener('click', this.bodyClick)
    }
    circleWillUnMount = () => {
        window.removeEventListener('click', this.bodyClick)
    }
    bodyClick = (e) => {
        if (e.target == document.body || e.target == this.spanTarget) {
            this.setState({ circleZIndex: 99 })
        } else {
            this.setState({ circleZIndex: -1 })
        }
    }
    render() {
        console.log(this.props);
        const style = {
            position: "fixed",
            top: "3vh",
            left: "60vw",
            width: "30vw",
            height: "80vh",
            zIndex: this.state.circleZIndex,
        }
        let { chat, addressList, square } = this.props.nav
        let { location: { pathname }, userData } = this.props
        if (pathname.indexOf("chatList") != -1) {
            this.charHref = pathname
        }
        if (pathname.indexOf("addressList") != -1) {
            this.addressHref = pathname
        }
        return (
            <div id='Nav'>
                <div className='head-portrait'>
                    <img src={userData.icon} alt="" />
                </div>
                <div title='聊天'>
                    <Badge count={chat}>
                        <NavLink to={this.charHref}>
                            <SolutionOutlined />
                        </NavLink>
                    </Badge>
                </div>
                <div title='通讯录'>
                    <Badge count={addressList}>
                        <NavLink to={this.addressHref}>
                            <MessageOutlined />
                        </NavLink>
                    </Badge>
                </div>
                {/* dot	不展示数字，只有一个小红点 */}
                <div title='微广场'>
                    <Badge dot={false} count={square}>
                        <DeploymentUnitOutlined onClick={this.showCircle} />
                    </Badge>
                </div>
                {this.state.showCircle ? <CircleOfFriends didMount={this.circleDidMount} willUnMount={this.circleWillUnMount} close={this.closeCircle} style={style} /> : ''}
            </div>
        )
    }
}
export default connect(
    state => ({
        nav: state.nav,
        userData: state.userData
    }),
    {
        initNavUnreadMessages
    }
)(withRouter(Nav))
