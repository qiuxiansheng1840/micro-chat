import React, { Component } from 'react'
import { Route, Switch, Redirect } from 'react-router-dom'
import PubSub from 'pubsub-js'
// 使用moudle.scss意味着使用模块化引入
import './App.scss'
import Nav from './containers/Nav/Nav'
import Main from './containers/Main/Main'
import List from './containers/List/List'
import Login from './containers/Login/Login'
import SignIn from './containers/SignIn/SignIn'
import Test from './components/test/Test'

export default class App extends Component {
  render() {
    const main = () => <>
      <Nav />
      <List />
      <Main />
      {/* <Test/> */}
    </>
    return (
      <div id='app'>
        <Switch>
          <Route path="/login" component={Login} />
          <Route path="/signin" component={SignIn} />
          <Route path="/" component={main} />
        </Switch>
      </div>
    )
  }
}

