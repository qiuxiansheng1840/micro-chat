import React, { Component } from 'react'
/**
 * 编辑框带表情
 * props要求：
 * isFocus：是否需要一渲染就focus到编辑框默认true
 * enterSend:是否回车发送数据，默认true
 * send：消息发送触发事件
 * clear:发送数据后是否清除内容，默认true
 * className:编辑框类名
 * placeholder:实现placeholder效果的文本(需要style配合 
 *                      className:empty::before {
                            content: attr(placeholder);
                        })
 */
export default class Edit extends Component {
    // 最后光标
    lastEditRange = null
    componentDidMount() {
        const { isFocus = true } = this.props
        if (isFocus) {
            this.focusEnd()
        }
    }
    // 清空内容
    clear = () => {
        this.editBox.innerHTML = ''
    }
    // 将鼠标聚焦到文末
    focusEnd = () => {
        this.editBox.focus()
        // 将光标聚焦到内容最后面
        const lastNode = this.editBox.lastChild
        if (lastNode) {
            const selection = window.getSelection();
            const rangeAt = selection.getRangeAt(0);
            rangeAt.setStartAfter(lastNode || null);
            selection.removeAllRanges();
            selection.addRange(rangeAt || null)
        }
    }
    // 向外输出我的innerHTML
    getInnerHTML = () => {
        return this.editBox.innerHTML
    }
    //让外面可以设置innerHTML
    setInnerHTML = (innerHtml) => {
        const { isFocus = true } = this.props
        this.editBox.innerHTML = innerHtml
        if (isFocus) {
            this.focusEnd()
        }
    }
    // 点击emoji时
    emojiClick = (emojiHtml) => {
        const selection = window.getSelection();
        // 输入框无光标，自动聚焦
        if (this.lastEditRange) {
            selection.removeAllRanges();
            selection.addRange(this.lastEditRange);
        } else {
            this.editBox.focus();
        }
        const rangeAt = selection.getRangeAt(0);
        const parentNode = document.createElement('div');
        parentNode.innerHTML = emojiHtml;
        // createdocumentfragment()方法创建了一虚拟的节点对象，节点对象包含所有属性和方法
        const emojiNode = document.createDocumentFragment();
        // 返回被添加的节点，即是emoji元素
        const lastNode = emojiNode.appendChild(parentNode.firstChild);
        rangeAt.insertNode(emojiNode || null);
        rangeAt.setStartAfter(lastNode || null);
        // rangeAt.collapse(true);
        // 清除现有光标，添加新光标
        selection.removeAllRanges();
        selection.addRange(rangeAt || null);
        // 记录最后光标对象
        this.lastEditRange = selection.getRangeAt(0)
    }
    // 点击编辑框时
    editBoxClick = (e) => {
        e.stopPropagation()
        // 记录当前光标位置
        this.lastEditRange = getSelection().getRangeAt(0)

    }
    // 输入时
    editBoxInput = (e) => {
        const { enterSend = true} = this.props
        if (enterSend) {
            if (e.nativeEvent.which == 13) {
                e.preventDefault();//☆阻止元素发生默认行为.阻止enter键回车换行
                e.stopPropagation();
                this.editBoxSend()
                e.returnValue = false;
            }
        }
    }
    // 失去焦点
    editBoxBlur = () => {
        this.lastEditRange = getSelection().getRangeAt(0)
    }
    // 发送编辑框数据（return HTML）
    editBoxSend = () => {
        const { clear = true, send = () => { } } = this.props
        send()
        if (clear) {
            this.clear()
            this.editBox.focus()
            this.lastEditRange = getSelection().getRangeAt(0)
        }
    }

    render() {
        return (
            <div
                className={this.props.className}
                ref={c => this.editBox = c}
                onClick={this.editBoxClick}
                onKeyPress={this.editBoxInput}
                contentEditable={true}
                placeholder={this.props.placeholder}
            >
            </div>
        )
    }
}
