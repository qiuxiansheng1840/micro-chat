import React, { Component } from 'react'
import './GroupHeadPortrait.scss'
export default class GroupHeadPortrait extends Component {
    render() {
        const { groupMemberList } = this.props
        return (
            <div className='groupHeadPortrait'>
                {
                    groupMemberList.map((groupMember) => {
                        return <div className='head-portrait' key={groupMember.id}>
                            <div>
                                <img src={groupMember.icon} alt="" />
                            </div>
                        </div>
                    })
                }
            </div>
        )
    }
}
