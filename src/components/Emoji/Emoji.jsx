import React, { Component } from 'react'
import './Emoji.scss'
import withScroll from '../../containers/with/withScroll'
class Emoji extends Component {
    myClickEvent=(code)=>{
        const {clickEvent} = this.props
        clickEvent && clickEvent(code)
    }
    render() {
        const { itemWidth, itemHeight, imgMaxWidth, imgWidth, imgHeight,itemPadding,code,url } = this.props
        console.log(this.props);
        return (
            <div className='emoji'>
                {
                    code.map((item,index) => {
                        return (<div onClick={ () =>this.myClickEvent(item) } key={index} style={{width:itemWidth,height:itemHeight}} className='emojiItem'>
                            <img style={{maxWeight:imgMaxWidth,width:imgWidth,height:imgHeight,padding:itemPadding}} src={url(item)} alt="" />
                        </div>)
                    })
                }
            </div>
        )
    }
}

export default withScroll(Emoji)