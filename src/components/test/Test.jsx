import { Input, Button } from 'antd'
import React, { Component } from 'react'
import './test.scss'
// import socket from '../../api/socketApi'
import storage from './../../util/storage'
export default class Test extends Component {
    click = () => {
        storage.setItem('id',this.id.input.value)
        storage.setItem('token',this.token.input.value)
        // try {
        //     socket.connection();
        // } catch (e) {
        //     // 捕获异常，防止js error
        //     // donothing
        // }
    }
    render() {
        return (
            <div id='test'>
                id:<Input ref={c => this.id = c}></Input>
                token:<Input ref={c => this.token = c}></Input>
                <Button onClick={this.click}>提交</Button>
            </div>
        )
    }
}
