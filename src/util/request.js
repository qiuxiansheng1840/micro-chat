/**
 * axios 二次封装
 */
import axios from "axios"
import config from './../config'
import {
    message
} from "antd"
import storage from './storage'

// 错误信息
const NETWORK_ERROR = '网络请求异常，请稍后重试'


// 创建axios实例对象，添加全局配置
const service = axios.create({
    baseURL: config.baseUrl,
    timeout: 8000,
})



// 请求拦截
service.interceptors.request.use((req) => {
    // 添加token
    const headers = req.headers;
    // 将token放进头里面
    if (!headers.authentication) {
        const token = (storage.getItem('userData') && storage.getItem('userData').token) || ''
        headers.authentication = token
    }
    return req;
})

// 响应拦截
service.interceptors.response.use((res) => {
    // 判断状态
    let status = res.status
    const {
        code,
        data,
        msg
    } = res.data;
    if (code === "200") {
        return data;

    } else if (code === "400") {
        return Promise.reject(msg)

        // ElMessage.error(msg)
        // setTimeout(() => {
        //     router.push('/login')
        // }, 1500)
        // return Promise.reject(msg)
    }else if (code === "406") {
        message.error(msg)


        // ElMessage.error(msg)
        // setTimeout(() => {
        //     router.push('/login')
        // }, 1500)
        // return Promise.reject(msg)
    } else if(code==='"401') {
        return new Error(res)
        // ElMessage.error(msg || NETWORK_ERROR);
        // return Promise.reject(msg || NETWORK_ERROR)
    }
})
/**
 * 请求的核心函数
 */
function request(options) {
    options.method = options.method || 'get'
    if (options.method.toLowerCase() === 'get') {
        options.params = options.data
    }

    // // 实现局部mock
    // let isMock = config.mock;
    // if (typeof options.mock != 'undefined') {
    //     isMock = options.mock;
    // }
    // if (config.env === 'production') {
    //     service.defaults.baseURL = config.baseApi
    // } else {
    //     service.defaults.baseURL = isMock ? config.mockApi : config.baseApi
    // }
    // console.log(options.method,config.mock);
    return service(options)
}
/**
 * 将request函数封装成request.get...的使用方式
 */
['get', 'post', 'put', 'delete', 'patch'].forEach((item) => {
    request[item] = (url, data, options) => {
        return request({
            url,
            data,
            method: item,
            ...options
        })
    }
})
export default request