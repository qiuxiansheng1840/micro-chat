export default {
    // 检查日期是否是今天的
    checkDateIsToday(date) {
        if ((new Date(date)).toDateString() === new Date().toDateString()) {
            return true
        }
        return null
    },
    // 检查日期是否是昨天的
    checkDateIsYesterday(date) {
        let currentTime = (new Date()); //当前时间
        let today = new Date(currentTime.getFullYear(), currentTime.getMonth(), currentTime.getDate()).getTime(); //今天凌晨
        let yesterday = new Date(today - 24 * 3600 * 1000).getTime();
        return date.getTime() < today && yesterday <= date.getTime();
    },
    // 检查日期是否是本周的
    checkDateIsThisWeek(date) {
        var currentTime = (new Date());
        var today = new Date(currentTime.getFullYear(), currentTime.getMonth(), currentTime.getDate()).getTime(); //今天凌晨
        var thisWeek = today - currentTime.getDay() * 24 * 3600 * 1000
        return date.getTime() >= thisWeek
    },
    /**
     * 检查两日期相差毫秒数
     * @param { string } previousDate 毫秒数 
     * @param { string } nextDate  毫秒数
     * @returns 相差的毫秒数
     */
    differenceBetweenTwoDates(previousDate, nextDate) {
        if (previousDate >= nextDate) {
            return null
        }
        return nextDate - previousDate
    },
    // 将毫秒数转换为分钟数
    MillisecondsToMinutes(millisecond) {
        return parseInt(millisecond / (1000 * 60)) ;
    },
    // 将毫秒数转换为天数、小时数、分钟数和秒数
    formatDuring(millisecond) {
        var days = parseInt(millisecond / (1000 * 60 * 60 * 24));
        var hours = parseInt((millisecond % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        var minutes = parseInt((millisecond % (1000 * 60 * 60)) / (1000 * 60));
        var seconds = (millisecond % (1000 * 60)) / 1000;
        return days + " 天 " + hours + " 小时 " + minutes + " 分钟 " + seconds + " 秒 ";
    }
}