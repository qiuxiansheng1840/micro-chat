import pinyin from "./pinyin";
export default {
    /**
     *
     * @param str 判断是否为中文
     * @returns {boolean} 中文返回true，其他返回false
     */
    isChinese(str) {
        var entryVal = str;
        var cnChar = entryVal.match(/[^\x00-\x80]/g);
        if (cnChar != null && cnChar.length > 0)
            return true;
        return false;
    },
    /**
     *
     * @param str 判断是否为英文
     * @returns {boolean} 英文返回true，其他返回false
     */
    isEnglish(str) {
        var entryVal = str;
        var enChar = entryVal.match(/^[A-Za-z]/);
        if (enChar != null && enChar.length > 0)
            return true;
        return false;
    },

    /**
     *获取拼音
     * @param str 字
     * @returns {*} 结果
     */
    arraySearch(str) {
        for (var name in pinyin) {
            if (pinyin[name].indexOf(str) != -1) {
                return name;
                break;
            }
        }
        return false;
    },

    /**
     *获取汉字拼音首字母
     * @param str 获取拼音的字符串
     * @param split 拼音分隔符
     * @param uppercase 是否转为大写
     * @returns {string} 结果
     */
    getPinYinFirstCharacter(str, split, uppercase) {
        split = split || " ";
        uppercase = uppercase || false;
        var len = str.length;
        var result = "";
        var reg = new RegExp('[a-zA-Z0-9\- ]');
        var val;
        var name;
        for (var i = 0; i < len; i++) {
            val = str.substr(i, 1);
            if (this.isChinese(val)) {
                name = this.arraySearch(val);
                if (reg.test(val)) {
                    result += split + val;
                } else if (name !== false) {
                    result += split + name.substring(0, 1);
                }
            } else {
                result += val;
            }
        }
        if (uppercase) result = result.toUpperCase();
        result = result.replace(split, "");
        return result.trim();
    },
    /**
     *获取字符串首字母
     * @param str 获取首字母的字符串
     * @param uppercase 是否转为大写
     * @param otherReturn = '#' 是否转为大写
     * @returns {string} 结果 如果是汉字返回拼音首字母、如果是数字或符号就默认返回'#'
     */
    getFirstLetter(str='',uppercase=true,otherReturn='#'){
        let firstLetter = str.slice(0, 1);
        if (this.isChinese(firstLetter)) {
            return this.getPinYinFirstCharacter(firstLetter, '', uppercase)
        }
        if (this.isEnglish(firstLetter)) {
            if(uppercase){
               return firstLetter.toUpperCase() 
            }else{
                return firstLetter
            }
        }
        return otherReturn
    }
}