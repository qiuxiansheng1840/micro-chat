/**
 * Strong二次封装
 */
import config from "../config";
export default{
    getStorage(){
        let storage = window.localStorage.getItem(config.namespace) || '{}'
        return JSON.parse(storage);
    },
    setItem(key,val){
        let storage = this.getStorage()
        storage[key]=val
        window.localStorage.setItem(config.namespace,JSON.stringify(storage))
    },
    getItem(key){
        return this.getStorage()[key]
    },
    clearItem(key){
        let storage = this.getStorage();
        delete storage[key];
        window.localStorage.setItem(config.namespace,JSON.stringify(storage))
    },
    clearAll(){
        window.localStorage.clear()
    }
}