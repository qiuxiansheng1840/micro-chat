import dateHandle from "./dateHandle"
import textHandle from "./textHandle"
import config from "../config"
// 求次幂
function pow1024(num) {
    return Math.pow(1024, num)
}
export default {
    /**
     * 计算聊天时间差并更换展示的字符串
     */
    CalculationTimeDifferenceForChat(previousDate, nextDate) {
        previousDate = Number(previousDate)
        nextDate = Number(nextDate)
        let nextDateObj = new Date(nextDate)
        let differenceValue = dateHandle.differenceBetweenTwoDates(previousDate, nextDate)
        let hoursMinutes = nextDateObj.getHours() + ':' + nextDateObj.getMinutes()
        if (differenceValue) {
            if (dateHandle.MillisecondsToMinutes(differenceValue) < 3) {
                return null
            }
            if (dateHandle.checkDateIsToday(nextDate)) {
                return hoursMinutes
            }
            if (dateHandle.checkDateIsYesterday(nextDateObj)) {
                return '昨天 ' + hoursMinutes
            }
            if (dateHandle.checkDateIsThisWeek(nextDateObj)) {
                let weekList = ['星期日', '星期一', '星期二', '星期三', '星期四', '星期五', '星期六']
                return weekList[nextDateObj.getDay()] + ' ' + hoursMinutes
            }
            return nextDateObj.getFullYear() + '年' + (nextDateObj.getMonth() + 1) + '月' + nextDateObj.getDate() + '日' + ' ' + hoursMinutes
        }
        return null
    },
    /**
     * 计算盆友圈时间差并更换展示的字符串
     * @param{string}//输入时间戳字符串
     */
    CalculationTimeDifferenceForCircle(innerDate) {
        innerDate = Number(innerDate)
        let innerDataObj = new Date(innerDate)
        let newDataString = Date.now()
        let differenceValue = dateHandle.differenceBetweenTwoDates(innerDate, newDataString)
        let hoursMinutes = innerDataObj.getHours() + ':' + innerDataObj.getMinutes()

        if (dateHandle.MillisecondsToMinutes(differenceValue) < 60) {
            return dateHandle.MillisecondsToMinutes(differenceValue) == 0 ? '刚刚' : dateHandle.MillisecondsToMinutes(differenceValue) + '分钟前'
        }
        if (dateHandle.checkDateIsToday(innerDate)) {
            return hoursMinutes
        }
        if (dateHandle.checkDateIsYesterday(innerDataObj)) {
            return '昨天 ' + hoursMinutes
        }
        return innerDataObj.getFullYear() + '/' + (innerDataObj.getMonth() + 1) + '/' + innerDataObj.getDate() + '/' + ' ' + hoursMinutes

    },
    /**
     * 将消息的HTML转换为String格式
     */
    HTMLToString(HTMLMessage) {
        let text = HTMLMessage.replaceAll('<br>', '\n');
        text = text.replaceAll('<img style="vertical-align: sub;" src="' + config.emojiUrl, '[')
        text = text.replaceAll('">', ']')
        return text
    },
    /**
     * 将消息的String转换为HTML格式
     */
    stringToHTML(stringMessage = '') {
        let text = stringMessage.replaceAll('\n', '<br>');
        // let text = stringMessage
        // .match(/\[.+?\]/g)获取[]内的emoji编码
        const emojiList = text.match(/\[.+?\]/g) || [] //改：不用【】
        emojiList.map((item) => {
            // 还有检查item是不是在emoji的数组中
            let emojiUrl = item.slice(1, item.length - 1)
            text = text.replace(`${item}`, `<img style="vertical-align: sub;" src="${config.emojiUrl}${emojiUrl}">`)
        })
        return text
    },
    /**
     * 清除复制来的样式
     * @param {Array} domList 
     */
    clearStyle(domList = []) {
        for (let index = 0; index < domList.length; index++) {
            const dom = domList[index];
            dom.style = '';
            if (dom.children.length > 0) {
                this.clearStyle(dom.children)
            }
        }
        return domList
    },
    /**
     * 通讯录排序
     */
    addressListSort(list = [], sortName = '') {
        const sortList = list
        sortList.sort((previous, next) => {
            if (textHandle.getFirstLetter(previous[sortName]) == '#') return 1
            if (textHandle.getFirstLetter(previous[sortName]) > textHandle.getFirstLetter(next[sortName])) {
                return 1
            }
            return -1
        })
        return sortList
    },
    //去除左右两端的空格
    trim(str = "") {
        return str.replace(/(^\s*)|(\s*$)/g, "");
    },
    /**
     * 将emojiCode转换为html
     */
    emojiCodeToHTML(emojiCode) {
        return `<img style="vertical-align: sub;" src="${config.emojiUrl}${emojiCode}">`
    },
    /**
     * 判断html是否代表空
     * @param{html}
     */
    HTMLIsNull(HTMLmessage) {
        let text = HTMLmessage.replaceAll('&nbsp;', '').replaceAll(' ', '').replaceAll('<div>', '').replaceAll('</div>', '').replaceAll('<br>', '')
        return text
    },
    getBase64(file) {
        return new Promise((resolve, reject) => {
            const reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = () => resolve(reader.result);
            reader.onerror = error => reject(error);
        });
    },
    filterSize(size) {
        if (!size) return '';
        if (size < pow1024(1)) return size + ' B';
        if (size < pow1024(2)) return (size / pow1024(1)).toFixed(2) + ' KB';
        if (size < pow1024(3)) return (size / pow1024(2)).toFixed(2) + ' MB';
        if (size < pow1024(4)) return (size / pow1024(3)).toFixed(2) + ' GB';
        return (size / pow1024(4)).toFixed(2) + ' TB'
    },


}